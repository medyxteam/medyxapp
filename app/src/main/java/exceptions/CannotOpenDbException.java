package exceptions;

/**
 * Created by francesco on 05/06/2017.
 */

public class CannotOpenDbException extends Exception {

    public CannotOpenDbException(String message){
        super(message);
    }

}
