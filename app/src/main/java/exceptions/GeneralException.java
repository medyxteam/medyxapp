package exceptions;

/**
 * Created by francesco on 09/03/2017.
 */

public class GeneralException extends Exception {

    public GeneralException(String message){
        super(message);
    }

}
