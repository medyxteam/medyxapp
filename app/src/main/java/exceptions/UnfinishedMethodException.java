package exceptions;

/**
 * Created by francesco on 07/03/2017.
 */

public class UnfinishedMethodException extends Exception {

    public UnfinishedMethodException(String message){
        super(message);
    }

}
