package exceptions;

/**
 * Created by francesco on 26/06/2017.
 */

public class FailedCommandException extends Exception {
    public FailedCommandException(String message){
        super(message);
    }
}
