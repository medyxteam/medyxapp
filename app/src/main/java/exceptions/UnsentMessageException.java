package exceptions;

/**
 * Created by francesco on 10/03/2017.
 */

public class UnsentMessageException extends Exception {
    public UnsentMessageException(String message ){
        super(message);
    }
}
