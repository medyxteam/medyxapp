package com.example.francesco.testservices.Views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.francesco.testservices.BuildConfig;
import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.R;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.NetworkManager;
import com.example.francesco.testservices.Views.Dialogs.SetPhoneNumberDialog;
import com.example.francesco.testservices.Views.Fragments.GeneralFragment;
import com.example.francesco.testservices.Views.Fragments.MessagesToSendFragment;
import com.example.francesco.testservices.Views.Fragments.SentMessagesFragment;

import java.util.ArrayList;
import java.util.List;

import RootUtilities.RootCommands;

public class TabsActivity extends AppCompatActivity implements SetPhoneNumberDialog.SetPhoneNumberDialogListener {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons={
            R.drawable.ic_tab_general,
            R.drawable.ic_tab_to_send,
            R.drawable.ic_tab_sent
    };

    public static String currentNumberStaticText = "Current: ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);
//        Toast.makeText(getApplicationContext(), "messaggio da mandare", Toast.LENGTH_SHORT).show();
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        Controller.onMainActivityCreation(getApplicationContext());
    //    NetworkManager.makeGetRequest("", getApplicationContext());
   //     Integer GET_url_code = NetworkManager.URL_GET_MESSAGES;
  //      NetworkManager.makeGetRequest("", getApplicationContext());
   //     Toast.makeText(getApplicationContext(), GET_url_code.toString(), Toast.LENGTH_SHORT).show();

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        if( !BuildConfig.FLAVOR.equals("development")){

            pref.edit().putString("getMessagesUrl",getResources().getString(R.string.getMessagesUrl_productionValue)).commit();
            pref.edit().putString("postReportUrl",getResources().getString(R.string.postReportUrl_productionValue)).commit();
        }
        else{
            RootCommands.DEBUG = true;
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager= (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setTabIcons();

    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new GeneralFragment(), "General");
        adapter.addFragment(new MessagesToSendFragment(), "To be sent");
        adapter.addFragment(new SentMessagesFragment(),"Sent");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void onDialogNegativeClick(android.app.DialogFragment dialog) {
        dialog.dismiss();
    }

    public void onDialogPositiveClick(android.app.DialogFragment dialog, View v) {
        Context context = this;
        EditText phoneNumberEditText = (EditText)v.findViewById(R.id.phoneNumber);
        String phoneNumber = phoneNumberEditText.getText().toString();
        if(Controller.isPhoneNumberValid(phoneNumber)){
            Controller.setPhoneNumber(context, phoneNumber);
            Toast.makeText(context,"Phone Number has been set", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
            TextView currentPhoneNumberTextView = (TextView) findViewById(R.id.currentNumberTextView);
            currentPhoneNumberTextView.setText(currentNumberStaticText + phoneNumber);
        } else{
            Toast.makeText(context,"Insert your National Code followed by the phone number", Toast.LENGTH_LONG).show();
            dialog.dismiss();
            SetPhoneNumberDialog newDialog = new SetPhoneNumberDialog();
            newDialog.show(getFragmentManager(),"SetPhoneNumberDialog");
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_items, menu);
        return true;
    }



    public void onGroupItemClick(MenuItem item) {
        // One of the group items (using the onClick attribute) was clicked
        // The item parameter passed here indicates which item it is
        // All other menu item clicks are handled by onOptionsItemSelected()
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_deleteDailyFile:
                // User chose the "Settings" item, show the app settings UI...
                if(Controller.deleteMessagesToSendFile()){
                    Snackbar snackbar = Snackbar.make(viewPager,"File deleted",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                else{
                    Snackbar snackbar = Snackbar.make(viewPager,"The file doesn't exists",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                return true;

            case R.id.action_openSettings:
                Intent intent = new Intent(this,SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_deleteLog:
                if(Controller.deleteLog()){
                    Snackbar snackbar = Snackbar.make(viewPager,"File deleted",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                else{
                    Snackbar snackbar = Snackbar.make(viewPager,"The file doesn't exists",Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                return true;
            case R.id.action_openLog:
                String logPath = Controller.getLogPath();
                if(logPath!= null){
                    Intent sendDataIntent = new Intent();
                    sendDataIntent.setAction(Intent.ACTION_EDIT);
                    Uri uri = Uri.parse("file://"+Controller.getLogPath());
                    sendDataIntent.setDataAndType(uri,"text/plain");
                    startActivity(sendDataIntent);
                }
                else {
                    Snackbar snackbar = Snackbar.make(viewPager, "Log doesn't exist", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
                //send Intent
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    public void setTabIcons(){
        if(tabLayout.getTabCount() != tabIcons.length){
            Logger.log("TabActivity","Cannot set icons; n. of tabs != n. of icons" ,true);
            return;
        }
        for(int i =0; i < tabLayout.getTabCount() ; i++  ){
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }
    }
}


