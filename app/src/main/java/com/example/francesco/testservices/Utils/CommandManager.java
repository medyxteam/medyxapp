package com.example.francesco.testservices.Utils;

import RootUtilities.Shell;
import RootUtilities.SimpleCommand;
import exceptions.FailedCommandException;

/**
 * Created by francesco on 21/06/2017.
 */

public class CommandManager {
    private static String TAG = "CommandManager";
    private ShellWrapper shellWrapper;
    private CommandOutputSaver commandOutputSaver;
    private Shell shell;

    public CommandManager(ShellWrapper shellWrapper, CommandOutputSaver commandOutputSaver){
        this.shellWrapper = shellWrapper;
        this.commandOutputSaver = commandOutputSaver;
        this.shell = null;
    }

    //made to replace whatsappManager's 'commandsTestOnClick' method

    public synchronized void executeSingleCommand(SimpleCommand command) throws FailedCommandException{
        this.executeSingleCommand(command, CommandOutputSaver.SAVE_NONE);
    }

    public synchronized void executeSingleCommand(SimpleCommand command, int outputSaveMode) throws FailedCommandException{
        this.executeSingleCommand(command, outputSaveMode, true);
    }

    public synchronized void executeSingleCommand(SimpleCommand command, int outputSaveMode, boolean closeShellAfter) throws FailedCommandException{
        try{
            this.commandOutputSaver.setSaveMode(outputSaveMode);
            if(this.shell == null){
                this.shell = shellWrapper.startRootShell();
            }
            shell.add(command).waitForFinish();
            if(closeShellAfter){
                this.closeShell();
            }
        }catch(Exception e){
            Logger.log(TAG,e.getMessage(),true);
            this.closeShell();
            throw new FailedCommandException(e.getMessage());
        }
    }

    public void closeShell(){
        try{
            if(this.shell != null){
                this.shell.close();
            }
        }catch(Exception e){
            Logger.log(TAG,e.getMessage(),true);

        }
        this.shell = null;
    }

    public boolean isShellClosed(){
        return (this.shell == null);
    }

    public CommandOutputSaver getCommandOutputSaver(){
        if(this.commandOutputSaver!= null){
            return this.commandOutputSaver;
        }
        else
            return new CommandOutputSaver(CommandOutputSaver.SAVE_LAST);
    }




}
