package com.example.francesco.testservices.Utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.example.francesco.testservices.Controllers.Controller;



public class ServiceScheduler {

    public static String TAG = "Scheduler";

    public static void scheduleService(Context context, Class c, Long activationTime){
        try {
            Log.i(c.getName(), "Proceeding to schedule next activation...");
            Intent intent = new Intent(context, c);
            PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmMgr.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP, activationTime, pendingIntent);
            Logger.log(TAG, c.getSimpleName() + " will activate "+
                    TimeUtilities.formatMillisToTime(activationTime -Controller.getElapsedRealTime())+" from now circa", true);
            String className = c.getSimpleName();
            switch(className){
                case "GetAndSaveMessagesService" :
                    Controller.getInstance().isGetAndSaveServiceScheduled = true;
                case "SendMessagesService":
                    Controller.getInstance().isSendMessagesServiceScheduled = true;
            }
        }
        catch (Exception e) {
            Logger.log(TAG, e.toString() + ". Service was: "+ c.getSimpleName(),true);
        }
    }
    public static void cancelAlarms(Context context, Class c){
        Intent intent = new Intent(context, c);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 1253, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        Logger.log(TAG, "Next " + c.getSimpleName() + " alarm was canceled", true);
        String className = c.getSimpleName();
        switch(className){
            case "GetAndSaveMessagesService" :
                Controller.getInstance().isGetAndSaveServiceScheduled = false;
            case "SendMessagesService":
                Controller.getInstance().isSendMessagesServiceScheduled = false;
        }

    }


    public static String getNextAlarmClock(Context appContext){
        return Settings.System.getString(appContext.getContentResolver(),Settings.System.NEXT_ALARM_FORMATTED);

    }
}
