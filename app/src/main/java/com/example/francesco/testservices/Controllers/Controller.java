package com.example.francesco.testservices.Controllers;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;

import com.example.francesco.testservices.Utils.CommandManager;
import com.example.francesco.testservices.Utils.CommandOutputSaver;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.NetworkManager;
import com.example.francesco.testservices.Utils.ServiceScheduler;
import com.example.francesco.testservices.Utils.ShellWrapper;
import com.example.francesco.testservices.Utils.TimeUtilities;

import org.json.JSONArray;

import java.io.File;
import java.util.Calendar;

import RootUtilities.RootCommands;
import RootUtilities.SimpleCommand;

/**
 * This class serves as the controller for our application. It's important cause it holds
 * the instance of the queue. It needs to be inizialized outside of the Services (threads)
 * for them to get the same instance of it when they call getInstance(). There shouldn't be any problem in our case
 * though, because the SendMessageService is always started for the first time by the GetAndSaveMessagesService
 * and the instance is visible inside the lower SendMessagesService thread.
 */

public class Controller {

    private static Controller instance = null;
    private MessagesQueue messagesQueue = null;
    private MessagesQueue sentMessages = null;
    public static boolean isTestEnvironment = false;
    private static Context context;
    private WhatsappManager whatsappManager;
    private CommandManager commandManager;
    public int getAndSaveServiceExecutions;
    public int sendMessagesServiceExecutions;
    public boolean isGetAndSaveServiceScheduled;
    public boolean isSendMessagesServiceScheduled;
    public static String TAG = "Controller";
    private static String sentIdsFileName = "sentMessagesIds.txt";
    private static String logFileName = "wa_log.txt";
    public static Long message_maxNegativeOffsetAllowed = -(30*60*1000L);

    private Controller(){
        Logger.log("Controller", "Initializing...", true);
        this.getAndSaveServiceExecutions = 0;
        this.sendMessagesServiceExecutions = 0;
        this.messagesQueue = new MessagesQueue();
        this.isGetAndSaveServiceScheduled = false;
        this.isSendMessagesServiceScheduled = false;
        this.commandManager = new CommandManager(new ShellWrapper(),new CommandOutputSaver(CommandOutputSaver.SAVE_LAST));
        this.whatsappManager = new WhatsappManager(getWaDbPath(), this.commandManager);

        setMessagesQueue();
    }

    public static synchronized Controller getInstance(){
        if( instance == null){
            instance = new Controller();
        }
        return instance;
    }
    public synchronized MessagesQueue getMessagesQueue(){
        return this.messagesQueue;
    }
    public synchronized MessagesQueue getSentMessages(){
        if(this.sentMessages == null) {
            this.sentMessages = FileUtilities.generateSentMessagesStack(getSentIdsFilePath(), this.whatsappManager);
        }
        return this.sentMessages;
    }
    public static String getSentIdsFileName(){
        return sentIdsFileName;
    }
    private static String getLogFileName(){
        return logFileName;
    }
    public static String getStoragePath(){
        return FileUtilities.getStoragePath();
    }
    public static String getWaDbPath(){
        return FileUtilities.getWaDbPath();
    }
    public static String getDailyMessagesFileName(){
        return "dailyMessages_" + getDate()+".json";
    }
    public static String getDate(){
        return TimeUtilities.getDate();
    }
    public static String getDailyPostData_FullPath(String phoneNumber){
        return getStoragePath()+ FileUtilities.POST_logs_folderName + File.separator + Controller.getPastDay() +".json";
    }
    private static String getPastDayLogNewPath(){
        return getStoragePath()+ FileUtilities.GENERAL_logs_folderName + File.separator + Controller.getPastDay() +".txt";
    }
    public static String getPastDay(){
        return TimeUtilities.getPastDay();
    }
    public static String getPhoneNumber(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        return pref.getString("phoneNumber","");
    }
    private static String getDailyMessagesStr(){
        return FileUtilities.getData(Controller.getStoragePath() + Controller.getDailyMessagesFileName());
    }
    public static Long getElapsedRealTime(){
        return TimeUtilities.getElapsedRealTime();
    }
    public static String getContactName(Context context, String phoneNumber) {
        ContentResolver cr = context.getContentResolver();
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor cursor = cr.query(uri, new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
        if (cursor == null){
            return null;
        }
        String contactName = null;
        if(cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME));
        }
        if(!cursor.isClosed()){
            cursor.close();
        }
        return contactName;
    }
    public WhatsappManager getWhatsappManager(){
        if(this.whatsappManager!= null){
            return this.whatsappManager;
        }
        else
            return new WhatsappManager(getWaDbPath(),this.commandManager);
    }



    public CommandManager getCommandManager(){
        return this.commandManager;
    }

    public synchronized void setMessagesQueue(){
        try{
            String dailyMessagesStr = getDailyMessagesStr();
            if (dailyMessagesStr != null)
                this.messagesQueue.setQueue(
                        new JSONArray(dailyMessagesStr),
                        FileUtilities.getFirstWordOfLastLine(getSentIdsFilePath()),
                        Calendar.getInstance().getTimeInMillis(),
                        message_maxNegativeOffsetAllowed,
                        this,
                        context
                );
        }
        catch(Exception e){
            Logger.log(TAG,e.getMessage(),true);
        }
    }

    public static boolean set_startServicesAfterBoot(Context context){
        String preferenceName = "startServicesAfterBoot";
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Boolean oldValue = pref.getBoolean(preferenceName, false);
        Boolean newValue = !oldValue;
        pref.edit().putBoolean(preferenceName, newValue).commit();
        if(newValue)
            Logger.log(TAG, "I servizi verranno avviati automaticamente dopo il boot", true);
        else
            Logger.log(TAG, "I servizi NON verranno avviati automaticamente dopo il boot", true);
        return newValue;
    }
    public static void setPhoneNumber(Context context, String phoneNumber){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        pref.edit().putString("phoneNumber",phoneNumber).commit();
        Logger.log(TAG,"Phone Number changed to:" + phoneNumber,true);
    }

    public boolean thereAreMessagesToSend(){
        return !(this.messagesQueue.isEmpty());
    }
    public boolean datesAreTheSame(Long date1, Long date2){
        return TimeUtilities.datesAreTheSame(date1,date2);
    }
    public static void deletePastDayDailyMessagesAndSentIdsFiles(){
        FileUtilities.deleteFile(getStoragePath()+ "dailyMessages_" + Controller.getPastDay()+".json" ) ;
        FileUtilities.deleteFile(getStoragePath() + Controller.getSentIdsFileName());
    }
    public static void cancelAlarms(Context context, Class c){
        ServiceScheduler.cancelAlarms(context, c);
    }
    public static void onMainActivityCreation(Context c){
        RootCommands.DEBUG = true;
        context = c;
        Logger.log(TAG+"/onMainActivityCreation","Creating the necessary files/folders",true);
        FileUtilities.createLogAndTestFolders();
    }
    public static boolean deleteMessagesToSendFile(){
        String fileName = getStoragePath() + getDailyMessagesFileName();
        return FileUtilities.deleteFile(fileName);
    }
    public static boolean deleteLog(){
        String fileName = getStoragePath() + getLogFileName();
        return FileUtilities.deleteFile(fileName);
    }
    public static String getLogPath(){
        return getStoragePath() + getLogFileName();
    }
    public static String getSentIdsFilePath(){
        return getStoragePath()+getSentIdsFileName();
    }
    public static boolean isPhoneNumberValid(String phoneNumber){
        return WhatsappManager.isPhoneNumberValid(phoneNumber);
    }
    public static void movePastDayLog() {
        File oldLog = new File(getLogPath());
        if (oldLog.exists()) {
            FileUtilities.renameFile(getLogPath(), getPastDayLogNewPath());
            FileUtilities.deleteFile(getLogPath());
        }
    }
    public boolean openAndKillWhatsapp(Context context){
        try {
            Logger.log(TAG+"/openAndKillWhatsapp","Opening whatsapp...",true);
            if(!openApp(context,"com.whatsapp")){
                Logger.log(TAG+"/openAndKillWhatsapp","Unable to open package com.whatsapp",true);
                return false;
            }
            Logger.log(TAG+"/openAndKillWhatsapp","Whatsapp started",true);
            Thread.sleep(45000);
            Logger.log(TAG+"/openAndKillWhatsapp", "Killing whatsapp...", true);
            SimpleCommand waKillCommand = new SimpleCommand("ps | grep -w 'com.whatsapp' | awk '{print $2}' | xargs kill");
            this.commandManager.executeSingleCommand(waKillCommand);
            Logger.log(TAG+"/openAndKillWhatsapp", "Whatsapp killed", true);
            return true;

        } catch (Exception e) {
            Logger.log(TAG, e.getMessage(), true);
            return false;
        }
    }
    private static boolean openApp(Context context, String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(packageName);
            if (i == null) {
                Logger.log("openApp","Package Name not found",true);
                return false;
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            return true;
        } catch (Exception e) {
            Logger.log("openApp",e.getMessage(),true);
            return false;
        }
    }
    public void messagesHaveBeenSent(){
        this.sentMessages = FileUtilities.generateSentMessagesStack(getSentIdsFilePath(),this.whatsappManager);
    }




}

