package com.example.francesco.testservices.Model;


import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Utils.CommandManager;
import com.example.francesco.testservices.Utils.CommandOutputSaver;
import com.example.francesco.testservices.Utils.Logger;

import java.util.Random;

import RootUtilities.SimpleCommand;



public class WhatsappManager implements MessageSender {
    public static String TAG = "WaUtils";
    private String dbPath;
    CommandManager commandManager;


    public WhatsappManager(String dbPath, CommandManager commandManager){
        this.dbPath = dbPath;
        this.commandManager = commandManager;
    }

    public synchronized void queryMessagesTable(String query, int outputSaveMode){
        try {
            SimpleCommand waKillCommand = new SimpleCommand("ps | grep -w 'com.whatsapp' | awk '{print $2}' | xargs kill");
            SimpleCommand queryCommand = new SimpleCommand("sqlite3 /data/data/com.whatsapp/databases/msgstore.db \"" + query + "; \" ");
            this.commandManager.executeSingleCommand(waKillCommand, CommandOutputSaver.SAVE_NONE, false);
            this.commandManager.executeSingleCommand(queryCommand, outputSaveMode, true);
        }
        catch( Exception e){
            Logger.log(TAG+"queryMessagesTable",e.getMessage(),true);
        }
    }




    public synchronized String retrieveStatus(String key_id){
        try {
            CommandOutputSaver commandOutputSaver = Controller.getInstance().getCommandManager().getCommandOutputSaver();
            commandOutputSaver.clearSavedOutput();
            if(dbPath == null || key_id == null)
                return null;
            String query = "select status from messages where key_id='"+key_id+"';";
            queryMessagesTable(query, CommandOutputSaver.SAVE_LAST);
            String status = commandOutputSaver.getSavedOutput();
            commandOutputSaver.clearSavedOutput();
            if(status.equals("0") || status.equals("4") || status.equals("5") || status.equals("13"))
                return status;
            else
                return "111";
        }
        catch( Exception e){
            Logger.log(TAG,e.getMessage(),true);
            return null;
        }
    }

    public boolean send(MessageToSend message){
        try{
            Boolean result = false;
            WhatsappMessage messageToSend = (WhatsappMessage)message;
            if(messageToSend.hasBeenSent()){
                Logger.log(TAG,"Message has already been sent",true);
                return true;
            }
            messageToSend.setWhatsappKey(sendWhatsappMessage(messageToSend.getNumber(),messageToSend.getMessage()));
            if(messageToSend.getWhatsappKey()!=null ){
                messageToSend.setStatus(retrieveStatus(messageToSend.getWhatsappKey()));
                String status = messageToSend.getStatusStr();
                result =status!=null &&
                        (status.equals("0")
                                || status.equals("4")
                                || status.equals("5")
                                || status.equals("13")
                        );
                messageToSend.setHasBeenSent(result);
            }else{
                Logger.log(TAG,"Database injection failed",true);
            }
            return result;
        }catch(Exception e){
            Logger.log(TAG,e.getMessage(),true);
            return false;
        }
    }


    public synchronized String sendWhatsappMessage(String number, String text) {
        try {
            if(!isPhoneNumberValid(number) || text ==""){
                return null;
            }
            long l1,l2;
            int k;
            String str1, str2,str3;
            Random localRandom = new Random(20L);
            SimpleCommand waKillCommand = new SimpleCommand("ps | grep -w 'com.whatsapp' | awk '{print $2}' | xargs kill");
            this.commandManager.executeSingleCommand(waKillCommand,CommandOutputSaver.SAVE_NONE,false);
            str3 = number + "@s.whatsapp.net";
            l1 = System.currentTimeMillis();
            l2 = l1 / 1000L;
            k = localRandom.nextInt();
            str1 = "sqlite3 /data/data/com.whatsapp/databases/msgstore.db \"INSERT INTO messages " +
                    "(key_remote_jid, key_from_me, key_id, status, needs_push, data, timestamp, MEDIA_URL, media_mime_type," +
                    " media_wa_type, MEDIA_SIZE, media_name , latitude, longitude, thumb_image, remote_resource," +
                    " received_timestamp, send_timestamp, receipt_server_timestamp, receipt_device_timestamp, raw_data," +
                    " media_hash, recipient_count, media_duration, origin)VALUES ('"
                    + str3 + "', 1,'" + l2 + "-" + k + "', 0,0, '" + text + "'," + l1
                    + ",'','', '0', 0,'', 0.0,0.0,'',''," + l1 + ", -1, -1, -1,0 ,'',0,0,0); \"";
            str2 = "sqlite3 /data/data/com.whatsapp/databases/msgstore.db \"insert into chat_list (key_remote_jid) " +
                    "select '" + str3
                    + "' where not exists (select 1 from chat_list where key_remote_jid='" + str3 + "');\"";
            str3 = "sqlite3 /data/data/com.whatsapp/databases/msgstore.db \"update chat_list" +
                    " set message_table_id = (select max(messages._id) from messages) " +
                    "where chat_list.key_remote_jid='" + str3 + "';\"";

            SimpleCommand permissionAdditionCommand = new SimpleCommand("chmod 777 /data/data/com.whatsapp/databases/msgstore.db");
            SimpleCommand messageInjectionCommand = new SimpleCommand(str1);
            SimpleCommand chatListInjectionCommand = new SimpleCommand(str2);
            SimpleCommand chatListUpdateCommand = new SimpleCommand(str3);
            this.commandManager.executeSingleCommand(permissionAdditionCommand,CommandOutputSaver.SAVE_NONE,false);
            this.commandManager.executeSingleCommand(messageInjectionCommand,CommandOutputSaver.SAVE_NONE,false);
            this.commandManager.executeSingleCommand(chatListInjectionCommand,CommandOutputSaver.SAVE_NONE,false);
            //once you start a series of commands, the last one has to have 'true' as parameter to let the shell
            // created by the commandManager be killed
            this.commandManager.executeSingleCommand(chatListUpdateCommand,CommandOutputSaver.SAVE_NONE,true);
            return l2+"-"+k;
        } catch (Exception e) {
            e.printStackTrace();
            Logger.log(TAG+"/sendWhatsappMessage", e.toString(), true);
        }
        return null;
    }
    public static boolean isPhoneNumberValid(String phoneNumber){
        int length= phoneNumber.length();
        return(length>= 11 && length <=12);
    }
}