package com.example.francesco.testservices.Views.Fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.R;
import com.example.francesco.testservices.Views.MessagesAdapter;

/**
 * Questa classe rappresenta il fragment con i messaggi giornalieri inviati. Viene gestito il settaggio
 * di un'apposita listView chiamata 'sentMessagesListview', che conterrà i vari messaggi, tramite
 * un'AsyncTask.
 */

public class SentMessagesFragment extends Fragment {

    public static boolean showSnackbar = false;
    public static boolean snackbarHasBeenShown = false;
    private ListView sentMessagesListView;
    private TextView noMessagesSentTextView;
    public SentMessagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sent_messages, container, false);
    }

    public void onStart(){
        super.onStart();
        noMessagesSentTextView = (TextView) getView().findViewById(R.id.noMessagesSentTextView);
        sentMessagesListView = (ListView)getView().findViewById(R.id.sentMessagesListView);
        //il recupero dei messaggi inviati, per essere effettivo, richiede di effettuare delle query sulla
        //tabella 'messages' di wa. Esse richiedono molto tempo quindi vengono effettuate in background
        new GetSentMessagesTask().execute();
     }

    public void onResume(){
        super.onResume();
        Snackbar snackbar = Snackbar.make(getView(),"No messages sent today",Snackbar.LENGTH_SHORT);
        if(!snackbarHasBeenShown && showSnackbar){
            snackbar.show();
            showSnackbar = false;
            snackbarHasBeenShown = true;
        }
    }


    public class GetSentMessagesTask extends AsyncTask<Void,Void, MessagesQueue> {

        protected void onPreExecute(){
        }

        protected MessagesQueue doInBackground(Void... values ){
            return Controller.getInstance().getSentMessages();
        }

        protected void onPostExecute(MessagesQueue result){
            if(result != null) {
                noMessagesSentTextView.setText("");
                sentMessagesListView.setAdapter(new MessagesAdapter(getContext(), result));
            }
                else{
                noMessagesSentTextView.setText("No messages sent yet!");
            }
        }
    }
}
