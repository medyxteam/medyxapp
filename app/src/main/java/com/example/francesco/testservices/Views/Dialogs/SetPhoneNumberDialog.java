package com.example.francesco.testservices.Views.Dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.example.francesco.testservices.R;


public class SetPhoneNumberDialog extends DialogFragment {

    public interface SetPhoneNumberDialogListener{
        void onDialogPositiveClick(DialogFragment dialog, View v);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    SetPhoneNumberDialogListener listener;

    @Override



    public void onAttach(Activity activity){
        super.onAttach(activity);
        try{
            listener = (SetPhoneNumberDialogListener) activity;
        }catch( ClassCastException e){
            throw new ClassCastException(activity.toString()
                    + " must implement SetPhoneNumberDialogListener");
        }
    }


    @Override
    public Dialog onCreateDialog(final Bundle savedInstance) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.dialog_set_phone_number, null);
        builder.setView(v)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDialogPositiveClick(SetPhoneNumberDialog.this, v);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        listener.onDialogNegativeClick(SetPhoneNumberDialog.this);
                    }
                });
        return builder.create();
    }
}