package com.example.francesco.testservices.Model;

import java.io.Serializable;



public class WhatsappMessage implements Serializable, MessageToSend {
    private String message;
    private String number;
    private Long date;
    private String id;
    private String whatsappKey;
    private boolean hasBeenSent;
    private int attemptsLeft;
    private String status;
    public static String TAG = "WhatsappMessage";

    public WhatsappMessage(String message, String number, Long date, String id){
        this.message = message;
        this.number = number;
        this.date = date;
        this.id = id;
        this.attemptsLeft = 3;
        this.whatsappKey = null;
        this.hasBeenSent = false;
        this.status = null;
    }

    public String getMessage(){
        return this.message;
    }
    public String getNumber(){
        return this.number;}
    public Long getDate(){
        return this.date;
    }
    public String getId() { return this.id;}
    public String getWhatsappKey() { return this.whatsappKey;}
    public int getAttemptsLeft() { return this.attemptsLeft;}
    public int getStatusInt(){
        return Integer.valueOf(this.status);
    }
    public String getStatusStr(){
        if(this.status != null)
            return this.status;
        else
            return "null";
    }
    public Boolean hasBeenSent() { return this.hasBeenSent;}

    public void setMessage(String m){
        this.message = m;
    }
    public void setNumber(String n){
        this.number = n;
    }
    public void setDate(Long d){
        this.date = d;
    }
    public void setId(String id){
        this.id = id;
    }
    public void setWhatsappKey(String whatsappKey){
        this.whatsappKey = whatsappKey;
    }
    public void setHasBeenSent(boolean hasBeenSent){
        this.hasBeenSent = hasBeenSent;
    }
    public void setStatus(String status){
        this.status = status;
    }

    public void decreaseAttemptsleft(){
        if (this.attemptsLeft >0 )
            this.attemptsLeft --;
    }


    }

