package com.example.francesco.testservices.Utils;

import android.os.SystemClock;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;


public class TimeUtilities {

    public static String TAG = "TimeUtils";

    public static String getPastDay() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd");
        return formatter.format(cal.getTime());
    }

    public static String getDate(){
        try{
            Calendar c = Calendar.getInstance();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy_MM_dd");
            return formatter.format(c.getTime());
        }
        catch(Exception e){
            Logger.log(TAG,e.getMessage(),true);
            return null;
        }
    }
    public static Long getElapsedRealTime(){
        return SystemClock.elapsedRealtime();
    }

    public static boolean datesAreTheSame(long date1, long date2){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(date1);
        int firstDate_Year = c.get(Calendar.YEAR);
        int firstDate_Month = c.get(Calendar.MONTH);
        int firstDate_Day = c.get(Calendar.DAY_OF_MONTH);
        c.setTimeInMillis(date2);
        int secondDate_Year = c.get(Calendar.YEAR);
        int secondDate_Month = c.get(Calendar.MONTH);
        int secondDate_Day = c.get(Calendar.DAY_OF_MONTH);
        return ((firstDate_Day == secondDate_Day)&&(firstDate_Month == secondDate_Month)&&(firstDate_Year == secondDate_Year));
    }

    public static String formatMillisToDate(Long millisDate){
        Date resultdate = new Date(millisDate);
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
        return sdf.format(resultdate);
    }

    public static String formatMillisToTime(Long millis){
        return String.format("%d min, %d sec",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
        );
    }

}
