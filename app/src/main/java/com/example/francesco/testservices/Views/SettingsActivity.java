package com.example.francesco.testservices.Views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.francesco.testservices.Views.Fragments.SettingsFragment;

/**
 * Questa è l'activity cui contenuto è il Fragment delle preferenze
 */

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
        //setContentView(R.layout.activity_settings);
    }
}
