package com.example.francesco.testservices.Model;



public interface MessageSender {
    public boolean send(MessageToSend messageToSend);
}
