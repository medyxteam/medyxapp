package com.example.francesco.testservices.Utils;

/**
 * This class offers methods to save and load data from the storage.
 */


import android.os.Environment;
import android.util.Log;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Model.WhatsappMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class FileUtilities {

    public static String TAG = "FileUtilities";
    public static String POST_logs_folderName = "POST_logs";
    public static String GENERAL_logs_folderName = "GENERAL_logs";
    private static String test_folderName= "Testing";
    public static synchronized String getData(String filePath) {
        try {
            File f = new File(filePath);
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException e) {
            Log.i("getData", "" + e.getLocalizedMessage());
            return null;
        }
    }
    public static String getStoragePath(){
        if(Controller.isTestEnvironment)
            return Environment.getExternalStorageDirectory() + File.separator+test_folderName+File.separator;
        else
            return Environment.getExternalStorageDirectory() + File.separator;
    }
    public static synchronized boolean saveData(String data, String filePath, Boolean append) {
        try {
            if(append){
                String str = getData(filePath);
                if (str == null){
                    append = false;
                }
                data = data + "\n";
            }
            FileWriter file = new FileWriter(filePath, append);
            file.write(data);
            file.flush();
            file.close();
            return true;
        } catch (IOException e) {
            Log.e("TAG", "Error while writing" + e.getLocalizedMessage());
            return false;
        }
    }
    public static synchronized boolean deleteFile(String f){
        try{
            File ff = new File(f);
            if (ff.exists()){
                if(ff.delete()){
                    Logger.log("deleteFile","File deleted:" + f, true);
                    return true;
                }
            } else
                Logger.log("deleteFile","File doesn't exists:" + f, true);
        } catch (Exception e){
            Logger.log("deleteFile","Warning: File not found:" + f, true);
        }
        return false;
    }
    public static String generateIdsAndStatusJsonStr(WhatsappManager waSender, String sentIdsFilePath){
        try {
            JSONObject ids = new JSONObject();
            JSONArray values = new JSONArray();
            String strLine;
            String status;
            FileInputStream fstream = new FileInputStream(sentIdsFilePath);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            while ((strLine = br.readLine()) != null ) {
                String[] row = strLine.split("\\s+");
                if(row.length == 2){
                    String key_id = row[1];
                    status = waSender.retrieveStatus(key_id);
                    JSONObject elem = new JSONObject();
                    elem.put("id",row[0]);
                    if(status == null){
                        Logger.log(TAG,"status not available for key_id "+key_id,true);
                        status = "110";
                    }else {
                        Logger.log(TAG, "status available for key_id: " + key_id + " status: " + status.toString(), true);
                    }
                    elem.put("status", status);
                    values.put(elem);
                } else{
                    if(row.length == 1){
                        JSONObject elem = new JSONObject();
                        elem.put("id",row[0]);
                        elem.put("status", "109");
                        Logger.log(TAG, "status not available for id " + row[0] + " add code 109", true);
                        values.put(elem);
                    } else{
                        JSONObject elem = new JSONObject();
                        elem.put("id","boh");
                        elem.put("status", "tuttoaputtane");
                        Logger.log(TAG, "status a puttane ", true);
                        values.put(elem);
                    }
                }
            }
            ids.put("ids",values);
            fstream.close();
            in.close();
            br.close();
            //try to return string of array of json
        //    return ids.toString();
            return values.toString();
        }
        catch(Exception e){
            return null;
        }
    }
    public static synchronized MessagesQueue generateSentMessagesStack(String sentIdsFilePath, WhatsappManager whatsappManager){
        String mName = "/generateSentMessages";
        try {
            CommandOutputSaver commandOutputSaver = Controller.getInstance().getCommandManager().getCommandOutputSaver();
            String strLine;
            Long date;
            MessagesQueue sentMessages = new MessagesQueue();
            if(!new File(sentIdsFilePath).exists()){
                Logger.log(TAG+mName,"Stack won't be generated because sentIdsFile does not exist",true);
                return null;
            }
            FileInputStream fstream = new FileInputStream(sentIdsFilePath);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            Logger.log(TAG+mName,"Setting the sent messages array...",true);
            while ((strLine = br.readLine()) != null ) {
                try{
                    String[] row = strLine.split("\\s+");
                    String id = row[0];
                    String query = "select key_remote_jid,data,receipt_server_timestamp from messages where key_id='"+row[1]+"';";
                    commandOutputSaver.clearSavedOutput();
                    whatsappManager.queryMessagesTable(query, CommandOutputSaver.SAVE_LAST);
                    String[] result = commandOutputSaver.getSavedOutput().split("\\|");
                    String number = result[0];
                    String messageData = result[1];
                    date = Long.parseLong(result[2]);
                    commandOutputSaver.clearSavedOutput();
                    WhatsappMessage sentMessage= new WhatsappMessage(messageData,number,date,id);
                    sentMessages.addMessage(sentMessage);
                }catch (Exception e){
                    fstream.close();
                    in.close();
                    br.close();
                    Logger.log(TAG+mName,e.getMessage(),true);
                }
            }
            fstream.close();
            in.close();
            br.close();
            return sentMessages;
        }
        catch(Exception e){
            Logger.log(TAG+mName,e.getMessage(),true);
            return null;
        }
    }
    public static String getFirstWordOfLastLine(String sentMessagesIdsFilePath){
        try {
            File sentMessagesIds = new File(sentMessagesIdsFilePath);
            if(!sentMessagesIds.exists()){
                Logger.log(TAG, "sentMessagesIds doesn't exists", true);
                return null;
            }
            FileInputStream fstream = new FileInputStream(sentMessagesIds);
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            String lastRow = null;
            String lastIdSent;
            while ((strLine = br.readLine()) != null) {
                lastRow = strLine;
            }
            String[] splitRow = lastRow.split("\\s+");
            lastIdSent = splitRow[0];
            return lastIdSent;
        } catch (Exception e) {
            Logger.log(TAG, "Error while catching the id of the last message sent", true);
            return null;
        }
    }
    public static void createLogAndTestFolders(){
        try{
            if(Controller.isTestEnvironment){
                File testRootFolder = new File(getStoragePath());
                if(!testRootFolder.exists()){
                    if(testRootFolder.mkdir()){
                        Logger.log(TAG, "Directory created: " + testRootFolder.getAbsolutePath(), true);
                    }
                    else{
                        Logger.log(TAG, "mkdir error: " + testRootFolder.getAbsolutePath(), true);
                    }
                }
            }

            String[] foldersPaths = {POST_logs_folderName,GENERAL_logs_folderName};
            for(String folderPath : foldersPaths){
                File folder = new File(Controller.getStoragePath() + folderPath);
                if(!folder.exists()) {
                    if(folder.mkdir()){
                        Logger.log(TAG, "Directory created: " + folder.getAbsolutePath(), true);
                    }
                    else
                        Logger.log(TAG, "mkdir error: " + folder.getAbsolutePath(), true);
                }
            }
        } catch(Exception e){
            Logger.log(TAG,e.getMessage(),true);
        }
    }
    public static void renameFile(String from, String to){
        try {
            Logger.log(TAG, "Renaming file:" + from , true);
            File ffrom = new File(from);
            if(!ffrom.exists()){
                Logger.log(TAG, "File " + from + " doesn't exists, so it won't be renamed", true);
                return;
            }
            File fto = new File(to);
            if (fto.exists()) {
                Logger.log(TAG, "File " + to + " already exists. It will be deleted", true);
                FileUtilities.deleteFile(to);
            }
            if(ffrom.renameTo(fto)){
                Logger.log(TAG, "File succesfully renamed", true);
            }
            else{
                Logger.log(TAG, "Error while renaming the file", true);
            }
        }
        catch (Exception e){
            Logger.log(TAG,e.getMessage(), true);
        }
    }

    public static String getWaDbPath(){
        return "/data/data/com.whatsapp/databases/msgstore.db";
    }


}


