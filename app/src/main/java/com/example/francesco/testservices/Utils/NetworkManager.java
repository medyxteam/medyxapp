package com.example.francesco.testservices.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.example.francesco.testservices.BuildConfig;
import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.R;

import java.io.IOException;

import RootUtilities.SimpleCommand;
import exceptions.GeneralException;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;

public class NetworkManager {
    private static final String TAG = NetworkManager.class.getSimpleName();
    public static int URL_GET_MESSAGES_WITHOUT_PARAMS = 2;
    public static int URL_GET_MESSAGES = 1;
    public static int URL_POST_REPORT = 0;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public NetworkManager() {}
    public static String getUrl(int code, Context context){
        String getMessagesUrl, postReportUrl;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(!BuildConfig.FLAVOR.equals("development")){
            Logger.log(TAG,"BuildConfig.DEBUG = false",true);
            getMessagesUrl = preferences.getString("getMessagesUrl",
                    context.getResources().getString(R.string.getMessagesUrl_productionValue));
            postReportUrl = preferences.getString("postReportUrl",
                    context.getResources().getString(R.string.postReportUrl_productionValue));
        }else{
            Logger.log(TAG,"BuildConfig.DEBUG = true",true);
            getMessagesUrl = preferences.getString("getMessagesUrl",
                    context.getResources().getString(R.string.getMessagesUrl_developmentValue));
            postReportUrl = preferences.getString("postReportUrl",
                    context.getResources().getString(R.string.postReportUrl_developmentValue));
        }
        switch (code){
            case 0 : return postReportUrl;
            case 1 : return addParametersToUrl(new String[]{"phoneNumber"},
                                new String[]{Controller.getPhoneNumber(context)},
                                getMessagesUrl);
            case 2 : return getMessagesUrl;
            //used for TESTING
            case 110 : return "http://www.google.it/ciao";
            case 111 : return "http://35.187.30.48:4500/test/webservices/therapies/getAllMessagesToSendEmpty";
            case 112 : return "";
            case 113 : return "";
        }
        return null;
    }


    public static String makeGetRequest(String reqUrl, Context c) {
        try {
            //add auth in get request -> that will become a post request;
            //change okHttp_Get in okHttp_Post;
        //    Toast.makeText(c, "prova chiamata GET", Toast.LENGTH_SHORT).show();
                String reqUrlAuth = c.getString(R.string.url_auth);
                String username = c.getString(R.string.username);
                String password = c.getString(R.string.password);
                RequestBody formBodyAuth = new FormBody.Builder().add("username", username).add("password", password).build();
                Response responseAuth = NetworkManager.okHttp_Post(reqUrlAuth, formBodyAuth);
                if(responseAuth.isSuccessful()){
                    String tkauth = responseAuth.body().string();
                    StringBuilder sb = new StringBuilder(tkauth);
                    sb.deleteCharAt(0);
                    sb.deleteCharAt(sb.length() - 1);
                    String tok = sb.toString();
                    String structure = c.getString(R.string.structure);
                    String tkauthjson = "{'token':" + tkauth + ",'structure':" + structure + "}";
                    String reqUrl2 = c.getString(R.string.url_get_messages);
                    RequestBody formData = new FormBody.Builder().add("token", tok).add("structure", structure).build();
                    Response response = okHttp_Post(reqUrl2, formData);
                    if(response.isSuccessful()){
                        String resultFromApiTest = response.body().string();
                        return resultFromApiTest;
                    }else return null;
                }else return null;
/*            Response response = okHtpp_Get(reqUrl);
            if(response.isSuccessful()){
                return response.body().string();
            } else return null;*/
        } catch (IOException e){
            Logger.log(TAG,e.toString(),false);
            return null;
        }
    }
    public static String makePostRequest(String reqUrl, String data, Context c){
        try{
            //add auth in post request;

                String reqUrlAuth = c.getString(R.string.url_auth);
                String username = c.getString(R.string.username);
                String password = c.getString(R.string.password);
 //               String dataAuthString = "{username:" + username + ",password:" + password + "}";
                RequestBody formBodyAuth = new FormBody.Builder().add("username", username).add("password", password).build();
                Response responseAuth = NetworkManager.okHttp_Post(reqUrlAuth, formBodyAuth);
                if(responseAuth.isSuccessful()){
                    String tkauth = responseAuth.body().string();
                    String structure = c.getString(R.string.structure);
                    StringBuilder sb = new StringBuilder(tkauth);
                    sb.deleteCharAt(0);
                    sb.deleteCharAt(sb.length() - 1);
                    String tok = sb.toString();
//                  String tkauthjson = "{'token':" + tkauth + ",'structure':" + structure + "," + data + "}";
                    RequestBody formData = new FormBody.Builder().add("token", tok).add("structure", structure).add("ids", data).build();
                    String reqUrl2 = c.getString(R.string.url_post_ids);
                    Response response = okHttp_Post(reqUrl2, formData);
                    if(response.isSuccessful()){
                        return response.body().string();
                    }else return null;
                }else return null;
            /*
            Response response = okHttp_Post(reqUrl, data);
            if(response.isSuccessful()){
                return response.toString();
            } else
                return null;*/
        } catch(Exception e){
            Logger.log(TAG,e.toString(),false);
            return null;
        }
    }
    public static boolean isNetworkAvailable(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNwInfo = connectivityManager.getActiveNetworkInfo();
        return activeNwInfo != null && activeNwInfo.isConnectedOrConnecting();
    }
    public static Response okHttp_Post(String url, RequestBody form) throws IOException{
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
            .url(url)
            .post(form)
            .build();
        try{
            Response r = client.newCall(request).execute();
            return r;
        }catch(IOException e){
            String error = e.toString();
            Logger.log(TAG,e.toString(),false);
            return null;
        }

    }
    public static Response okHtpp_Get(String url) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
            .url(url)
            .build();

        return client.newCall(request).execute();
    }
    public static String addParametersToUrl(String[] names, String[] values, String url){
        try{
            if( names== null || values == null || names.length!= values.length){
                throw new GeneralException("Error while building the url");
            }
            url = url.concat("?");
            int length = names.length;
            for(int i=0; i < length ; i++){
                url = url.concat(names[i]+"="+values[i]);
                if(i < (length -1)){
                    url = url.concat("&");
                }
            }
            return url;
        } catch(Exception e){
            Logger.log(TAG,e.toString(),true);
            return url;
        }
    }
    public static String sendYesterdayIdsToServer(String url,String jsonStr,String phoneNumber,int numberOfAttempts, Context c){
        Logger.log(TAG, "Proceeding to POST the sent messages ids...", true);
        String postResult = null;
        while(jsonStr != null && numberOfAttempts > 0){
            postResult = makePostRequest(url, jsonStr, c);
            if (postResult != null){
                Logger.log(TAG, "Post request was made succesfully. Response: "+postResult, true);
                break;
            } else{
                Logger.log(TAG, "Error while executing POST request." +
                        " Response:" + postResult +"Retrying...", true);
                numberOfAttempts--;
            }
        }
        if(postResult == null) {
            Logger.log(TAG, "No attempts left or json string null; post request failed.", true);
        }
        FileUtilities.saveData(jsonStr, Controller.getDailyPostData_FullPath(phoneNumber), true);
        Logger.log(TAG, "post data: " + postResult, true);
        return postResult;
    }
    public static synchronized boolean ping(CommandManager commandManager){
        SimpleCommand command = new SimpleCommand("ping -c 1 8.8.8.8");
        try{
            commandManager.executeSingleCommand(command);
            return true;
        }
        catch (Exception e){
            Logger.log(TAG,e.getMessage(),true);
            return false;
        }
    }
    public static void keepConnectionAlive(Context context, CommandManager commandManager){
        try{
            if(isNetworkAvailable(context)){
                Logger.log(TAG,"CONNECTION IS AVAILABLE",true);
            } else{
                Logger.log(TAG,"CONNECTION IS --not-- AVAILABLE", true);
            }if(ping(commandManager)){
                Logger.log(TAG,"Ping done", true);
            } else{
                Logger.log(TAG,"Ping failed", true);
            }
        } catch(Exception e){
            Logger.log(TAG,e.getMessage(), true);
        }
    }
}
