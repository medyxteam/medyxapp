package com.example.francesco.testservices.Views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.R;
import com.example.francesco.testservices.Utils.TimeUtilities;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Adattatore realizzato per la visualizzazione dei messaggi
 */

public class MessagesAdapter extends BaseAdapter {

    Context context;
    MessagesQueue queue;
    private static LayoutInflater inflater = null;

    public MessagesAdapter(Context context, MessagesQueue queue){
        this.context = context;
        this.queue = queue;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount(){
        return queue.size();
    }

    public Object getItem(int position){
        return this.queue.getMessage(position);

    }

    public long getItemId(int position){
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        String sender;
        View vi = convertView;
        if(vi == null)
            vi = inflater.inflate(R.layout.message_row, null);
        TextView dateView = (TextView) vi.findViewById(R.id.date);
        TextView numberView = (TextView) vi.findViewById(R.id.number);
        TextView messageTextView = (TextView) vi.findViewById(R.id.messageText);
        messageTextView.setText(queue.getMessage(position).getMessage());
        String contactName = Controller.getContactName(context,queue.getMessage(position).getNumber() );
        if(contactName == null)
            sender = queue.getMessage(position).getNumber();
        else
            sender = contactName;
        numberView.setText(sender);
        Long date = queue.getMessage(position).getDate();
        Date resultdate = new Date(date);
        if( TimeUtilities.datesAreTheSame(date, Calendar.getInstance().getTimeInMillis()) ){
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            dateView.setText("Oggi, " + sdf.format(resultdate));
        }else {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, HH:mm");
            dateView.setText(sdf.format(resultdate).toString());
        }
        return vi;
    }

}
