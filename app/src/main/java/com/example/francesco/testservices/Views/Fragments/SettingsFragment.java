package com.example.francesco.testservices.Views.Fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.example.francesco.testservices.R;

/**
 * Questo è il fragment per che contiene le preferenze, accedibile cliccando "Settings" all'interno
 * del menù della toolbar
 */

public class SettingsFragment extends PreferenceFragment{


    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }


}



