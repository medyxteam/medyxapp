package com.example.francesco.testservices.Services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Utils.CommandManager;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.NetworkManager;
import com.example.francesco.testservices.Utils.ServiceScheduler;

import java.io.File;

/**
 * This service is meant to :
 * 1) Get the daily list of messages to send from the url put as extra inside the intent by the main activity.
 *    1.1) If the daily file doesn't exists, the service tries to make a POST request with the messages' ids and status
 * 2) Start "SendMessagesService"
 * 3) Reschedule its next activation through the alarm manager
 */

public class GetAndSaveMessagesService extends IntentService {

    public GetAndSaveMessagesService() {
        super("GetAndSaveMessagesService");
    }

    public static String TAG = "GetAndSaveMessages";
    public static Long standardOffset = 15*60*1000L;
    private int POST_attempts = 2;
    Long nextActivationTime;
    CommandManager commandManager;
    WhatsappManager whatsappManager;
    public void onHandleIntent(Intent intent) {

        Context context = getApplicationContext();

        Integer GET_url_code = getUrlCode(intent.getExtras(),"GET_urlCode");
//        String jsonResponse2 = NetworkManager.makeGetRequest(NetworkManager.getUrl(GET_url_code, getApplicationContext()), getApplicationContext());

        try {
//            Toast.makeText(context, "handle intent", Toast.LENGTH_SHORT).show();
            Controller.getInstance().getAndSaveServiceExecutions++;
            ServiceScheduler.cancelAlarms(getApplicationContext(), GetAndSaveMessagesService.class);
            ServiceScheduler.cancelAlarms(getApplicationContext(), SendMessagesService.class);
            commandManager = Controller.getInstance().getCommandManager();
            whatsappManager = Controller.getInstance().getWhatsappManager();
            NetworkManager.keepConnectionAlive(getApplicationContext(), commandManager);
            String dailyFileName = Controller.getStoragePath() + Controller.getDailyMessagesFileName();
            String dailyFile = FileUtilities.getData(dailyFileName);
            if (dailyFile == null ){
                Logger.log(TAG,"Daily file doesn't exist. ", true);
                if(new File(Controller.getSentIdsFilePath()).exists()  ){
                    NetworkManager.sendYesterdayIdsToServer(
                            NetworkManager.getUrl(NetworkManager.URL_POST_REPORT, context),
                            FileUtilities.generateIdsAndStatusJsonStr(whatsappManager,Controller.getSentIdsFilePath()),
                            Controller.getPhoneNumber(context),
                            POST_attempts,
                            getApplicationContext()
                    );
                    Thread.sleep(10*1000);
                }
                Controller.deletePastDayDailyMessagesAndSentIdsFiles();
                Controller.movePastDayLog();
                Logger.log(TAG, "Proceeding to execute get request...", true);
                String jsonResponse = NetworkManager.makeGetRequest(NetworkManager.getUrl(GET_url_code, getApplicationContext()), getApplicationContext());
                if (jsonResponse != null) {
                    Logger.log(TAG,"json response ok", true);
                    FileUtilities.saveData(jsonResponse, dailyFileName, false);
                    Toast.makeText(getApplicationContext(), "get", Toast.LENGTH_SHORT).show();
                    Controller.getInstance().setMessagesQueue();
                } else {
                    Logger.log(TAG,"json response: null. The service will reschedule itself and stop.", true);
                    nextActivationTime = Controller.getElapsedRealTime() + standardOffset;
                    ServiceScheduler.scheduleService(getApplicationContext(), GetAndSaveMessagesService.class, nextActivationTime);
                    return;
                }
            } else
                Logger.log(TAG, "Daily file exists", true);
            if(Controller.getInstance().thereAreMessagesToSend()){
//                Toast.makeText(getApplicationContext(), "messaggio da mandare", Toast.LENGTH_SHORT).show();
                Intent sendMessagesIntent = new Intent(getApplicationContext(), SendMessagesService.class);
                startService(sendMessagesIntent);
            } else
                Logger.log(TAG, "No messages to send. SendMessagesService will not get called", true);
        } catch (Exception e) {
            Logger.log(TAG, e.toString(), true);
        }
        nextActivationTime = Controller.getElapsedRealTime()+ standardOffset;
        ServiceScheduler.scheduleService(getApplicationContext(),GetAndSaveMessagesService.class, nextActivationTime);
    }

    public int getUrlCode(Bundle extras,String key){
        int code;
        if(extras != null){
            code = extras.getInt(key);
            if(code !=0){
                return code;
            }
        }
        return NetworkManager.URL_GET_MESSAGES;
    }


}




