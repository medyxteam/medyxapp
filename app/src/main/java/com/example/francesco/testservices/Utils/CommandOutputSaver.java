package com.example.francesco.testservices.Utils;

/**
 * Created by francesco on 20/06/2017.
 */

public class CommandOutputSaver {
    public static final int SAVE_ALL = 1;
    public static final int SAVE_LAST = 0;
    public static final int SAVE_NONE = -1;
    private int saveMode;
    private String commandOutput;

    public CommandOutputSaver(int saveMode){
        this.commandOutput = "";
        this.saveMode = saveMode;
    }



    public void saveOutput(String output){
        switch(this.saveMode){
            case SAVE_LAST :
                this.commandOutput = output;
                break;
            case SAVE_ALL :
                this.commandOutput = this.commandOutput +" ".concat(output);
                break;
            case SAVE_NONE :
                return;
        }
    }

    public void setSaveMode(int saveMode){
        if(saveMode == SAVE_ALL || saveMode == SAVE_NONE || saveMode == SAVE_LAST)
            this.saveMode = saveMode;

    }

    public int getSaveMode(){
        return this.saveMode;
    }


    public void clearSavedOutput(){
        this.commandOutput = "";
    }

    public String getSavedOutput(){
        String output = this.commandOutput;
        this.clearSavedOutput();
        return output;
    }
}


