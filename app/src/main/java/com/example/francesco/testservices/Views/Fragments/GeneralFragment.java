package com.example.francesco.testservices.Views.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.R;
import com.example.francesco.testservices.Services.GetAndSaveMessagesService;
import com.example.francesco.testservices.Services.SendMessagesService;
import com.example.francesco.testservices.Views.Dialogs.SetPhoneNumberDialog;
import com.example.francesco.testservices.Views.TabsActivity;

/**
 * Questo è il fragment che, per ora, viene visualizzato quando viene lanciata l'activity contente i vari fragment.
 * Contiene i bottoni per l'avvio e per lo stop dei servizi, e per il settaggio
 * del numero di telefono (che ancora non è obbligatorio
 */

public class GeneralFragment extends Fragment implements View.OnClickListener {



    public GeneralFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_general, container, false);

        ImageView startServicesImageView = (ImageView) view.findViewById(R.id.startServices_imageView);
        startServicesImageView.setOnClickListener(this);

        ImageView stopServicesImageView = (ImageView) view.findViewById(R.id.stopServices_imageView);
        stopServicesImageView.setOnClickListener(this);

        Button setPhoneNumberButton = (Button) view.findViewById(R.id.setPhoneNumberButton);
        setPhoneNumberButton.setOnClickListener(this);

        final CheckBox bootCheckBox = (CheckBox) view.findViewById(R.id.bootCheckBox);
        bootCheckBox.setOnClickListener(this);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getContext());

        boolean startOnBoot = pref.getBoolean("startServicesAfterBoot", false);
        bootCheckBox.setChecked(startOnBoot);
        TextView currentPhoneNumberTextView = (TextView)view.findViewById(R.id.currentNumberTextView);
        currentPhoneNumberTextView.setText(TabsActivity.currentNumberStaticText+ Controller.getPhoneNumber(getContext()));
        return view;

    }

    public void onClick(View view){
        switch(view.getId()){
            case R.id.bootCheckBox:
                onBootCheckBoxClicked(view);
                break;
            case R.id.setPhoneNumberButton:
                onSetPhoneNumberButtonClicked();
                break;
            case R.id.startServices_imageView:
                onStartServicesButtonClicked();
                break;
            case R.id.stopServices_imageView:
                onStopServicesButtonClicked();
        }

    }

    public void onStartServicesButtonClicked(){
        Context context = getContext();
        Intent getDailyFileIntent = new Intent(context, GetAndSaveMessagesService.class);
        context.startService(getDailyFileIntent);
        Toast toast = Toast.makeText(context, "Services started", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void onStopServicesButtonClicked(){
        Context context = getContext();
        Controller.cancelAlarms(context, GetAndSaveMessagesService.class);
        Controller.cancelAlarms(context, SendMessagesService.class);
        Toast toast = Toast.makeText(context, "Services stopped", Toast.LENGTH_SHORT);
        toast.show();
    }

    public void onSetPhoneNumberButtonClicked(){
        SetPhoneNumberDialog dialog = new SetPhoneNumberDialog();
        dialog.show(getActivity().getFragmentManager(),"SetPhoneNumberDialog");
    }

    public void onBootCheckBoxClicked(View view) {
        boolean hasToBeChecked = Controller.set_startServicesAfterBoot(getContext());
        ((CheckBox) view).setChecked(hasToBeChecked);

    }



}

