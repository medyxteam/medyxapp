package com.example.francesco.testservices.Utils;

import RootUtilities.Shell;

/**
 * Created by francesco on 16/06/2017.
 */

public class ShellWrapper{
    public Shell startRootShell(){
        try{
            return Shell.startRootShell();
        }
        catch(Exception e){
            return null;
        }
    }
}

