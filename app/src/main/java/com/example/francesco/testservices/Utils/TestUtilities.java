package com.example.francesco.testservices.Utils;

import android.util.Log;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.Model.WhatsappMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.HashSet;

public class TestUtilities {

    public static String TAG = "TestUtilities";
    public static String VALID_MESSAGE_STATUS_CODE = "01";
    public static String INVALID_MESSAGE_STATUS_CODE = "05";
    public static String fakeIdsFilePath = Controller.getStoragePath() + "fakeSentIds.txt";
    public static String sentIdsFilePath = Controller.getSentIdsFilePath();
    public static int URL_GET_INVALID  = 110;
    public static int URL_GET_BLANK_JSON = 111;
    public static int URL_GET_LATE_MESSAGES = 112;
    public static int URL_GET_EMPTY_ARRAY = 113;


    public static JSONArray buildBigDelaysArray(){
        JSONArray array = new JSONArray();
        try{
            JSONObject obj1 = new JSONObject();
            obj1.put("id","01");
            obj1.put("message","Messaggio valido 1");
            obj1.put("patientNumber","393450154761");
            obj1.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() +16*60* 1000));
            obj1.put("testStatus",VALID_MESSAGE_STATUS_CODE);
            array.put(obj1);

            JSONObject obj2 = new JSONObject();
            obj2.put("id","02");
            obj2.put("message","Messaggio valido 2");
            obj2.put("patientNumber","393450154761");
            obj2.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() + 30*60*1000 ));
            obj2.put("testStatus",VALID_MESSAGE_STATUS_CODE);
            array.put(obj2);

        }
        catch(Exception e){

        }
        return array;
    }

    public static JSONArray buildSmallDelaysArray(){

        JSONArray array = new JSONArray();
        try{
            JSONObject obj1 = new JSONObject();
            obj1.put("id","01");
            obj1.put("message","Messaggio valido 1");
            obj1.put("patientNumber","393450154761");
            obj1.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() +20* 1000));
            obj1.put("testStatus",VALID_MESSAGE_STATUS_CODE);
            array.put(obj1);

            JSONObject obj2 = new JSONObject();
            obj2.put("id","02");
            obj2.put("message","Messaggio valido 2");
            obj2.put("patientNumber","393450154761");
            obj2.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() + 100* 1000 ));
            obj2.put("testStatus",VALID_MESSAGE_STATUS_CODE);
            array.put(obj2);

        }
        catch(Exception e){

        }
        return array;
    }


    public static JSONArray buildTestArray(){
        try{
            /**
             * i messaggi validi hanno 'testStatus' = '01', gli altri '05'
             */

            JSONArray array = new JSONArray();

            JSONObject obj1 = new JSONObject();
            obj1.put("id","01");
            obj1.put("message","Messaggio valido 1");
            obj1.put("patientNumber","393450154761");
            obj1.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() +5* 6000));
            obj1.put("testStatus",VALID_MESSAGE_STATUS_CODE);
            array.put(obj1);

            JSONObject obj2 = new JSONObject();
            obj2.put("id","02");
            obj2.put("message","Messaggio valido 2");
            obj2.put("patientNumber","393450154761");
            obj2.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() + 5* 8000 ));
            obj2.put("testStatus",VALID_MESSAGE_STATUS_CODE);
            array.put(obj2);

            //passato piu di mezz'ora
            JSONObject obj3 = new JSONObject();
            obj3.put("id","03");
            obj3.put("message","Messaggio non valido");
            obj3.put("patientNumber","33333333333");
            obj3.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() + Controller.message_maxNegativeOffsetAllowed ));
            obj3.put("testStatus",INVALID_MESSAGE_STATUS_CODE);
            array.put(obj3);

            //data diversa
            JSONObject obj4 = new JSONObject();
            obj4.put("id","04");
            obj4.put("message","Messaggio non valido");
            obj4.put("patientNumber","33333333333");
            obj4.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() - 24 *60  * 60 * 1000));
            obj4.put("testStatus",INVALID_MESSAGE_STATUS_CODE);
            array.put(obj4);

            JSONObject obj7 = new JSONObject();
            obj7.put("id","07");
            obj7.put("message","Messaggio valido 3");
            obj7.put("patientNumber","393450154761");
            obj7.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() + 10* 8000));
            obj7.put("testStatus",VALID_MESSAGE_STATUS_CODE);
            array.put(obj7);

            //campo 'message' non presente
            JSONObject obj8 = new JSONObject();
            obj8.put("id","08");
            obj8.put("patientNumber","33333333333");
            obj8.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() + 10* 8000 ));
            obj8.put("testStatus",INVALID_MESSAGE_STATUS_CODE);
            array.put(obj8);

            //'patientNumber' non presente
            JSONObject obj9 = new JSONObject();
            obj9.put("id","09");
            obj9.put("date",String.valueOf(Calendar.getInstance().getTimeInMillis() + 10* 8000 ));
            obj9.put("testStatus",INVALID_MESSAGE_STATUS_CODE);
            array.put(obj9);

            //date not long
            JSONObject obj10 = new JSONObject();
            obj10.put("id","10");
            obj10.put("message","Messaggio non valido");
            obj10.put("patientNumber","33333333333");
            obj10.put("date","monday 1 2 3");
            obj10.put("testStatus",INVALID_MESSAGE_STATUS_CODE);
            array.put(obj10);

            //date missing
            JSONObject obj11 = new JSONObject();
            obj11.put("id","11");
            obj11.put("message","Messaggio non valido");
            obj11.put("patientNumber","33333333333");
            obj11.put("testStatus",INVALID_MESSAGE_STATUS_CODE);
            array.put(obj11);

            //same date but negative offset exceeded
            JSONObject obj12 = new JSONObject();
            obj12.put("id","12");
            obj12.put("message","Messaggio non valido");
            obj12.put("patientNumber","33333333333");
            obj12.put("date", Calendar.getInstance().getTimeInMillis() + Controller.message_maxNegativeOffsetAllowed);
            obj12.put("testStatus",INVALID_MESSAGE_STATUS_CODE);
            array.put(obj12);

            return array;
        }
        catch(Exception e){
            return null;
        }
    }
    public static boolean queueContainsValidElementsOnly(JSONArray array, MessagesQueue queue){
        HashSet<String> validIds = getValidMessagesIds(array);
        if( validIds == null || validIds.isEmpty())
            return false;
        for(int i = 0; i < queue.size() ; i++){
            WhatsappMessage m = queue.getMessage(i);
            if( validIds.contains(m.getId())){
                validIds.remove(m.getId());
            }
            else
                return false;
        }
        return validIds.isEmpty();
    }

    public static HashSet<String> getValidMessagesIds(JSONArray array){
        try{
            HashSet<String> validIds = new HashSet<>();
            for(int i = 0; i < array.length(); i++){
                if(array.getJSONObject(i).getString("testStatus").equals("01")){
                    validIds.add( array.getJSONObject(i).getString("id"));
                }
            }
            return validIds;
        }
        catch(Exception e){
            return null;
        }
    }

    public static void deleteOldFiles(){
        FileUtilities.deleteFile(sentIdsFilePath);
        FileUtilities.deleteFile(Controller.getStoragePath()+Controller.getDailyMessagesFileName());
        FileUtilities.deleteFile(Controller.getStoragePath()+"GENERAL_logs/"+Controller.getPastDay()+".txt");
        FileUtilities.deleteFile(Controller.getStoragePath()+"POST_logs/" + Controller.getPastDay()+".json");
    }

    public static void createPastDayMessagesFiles(){
        try{
            FileUtilities.saveData(" ",Controller.getStoragePath()+"wa_log.txt",true);
            FileUtilities.saveData(
                    TestUtilities.buildTestArray().toString(),
                    Controller.getStoragePath()+"dailyMessages_"+Controller.getPastDay()+".json",false );
            FileUtilities.deleteFile(sentIdsFilePath);
            FileUtilities.saveData("01 xxx",sentIdsFilePath, true);
            FileUtilities.saveData("02",sentIdsFilePath, true);
            FileUtilities.saveData("03 xxx xxx",sentIdsFilePath, true);
        }
        catch(Exception e){
            Logger.log(TAG,e.getMessage(),true);
        }
    }

    public static HashSet<String> generateSentMessagesIdsSet(String sentMessagesIdsPath) {
        try {
            HashSet<String> idsSet = new HashSet<>();
            if(sentMessagesIdsPath== null || !new File(sentMessagesIdsPath).exists()){
                Logger.log(TAG,"sentIdsFile doesn't exists",true);
                return null;
            }
            FileInputStream fstream = new FileInputStream(new File(sentMessagesIdsPath));
            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null ) {
                String[] row = strLine.split("\\s+");
                idsSet.add(row[0]);
            }
            return idsSet;
        } catch (Exception e) {
            Log.e(TAG, "Error while creating the ids set (presumably while reading through the file):", e);
            return null;
        }
    }

    public static HashSet<String> buildColumnNamesSet(){
        HashSet<String> names = new HashSet<>();
        names.add("_id");
        names.add("key_remote_jid");
        names.add("key_from_me");
        names.add("key_id");
        names.add("status");
        names.add("needs_push");
        names.add("data");
        names.add("timestamp");
        names.add("media_url");
        names.add("media_mime_type");
        names.add("media_wa_type");
        names.add("media_size");
        names.add("media_name");
        names.add("media_caption");
        names.add("media_hash");
        names.add("media_duration");
        names.add("origin");
        names.add("latitude");
        names.add("longitude");
        names.add("thumb_image");
        names.add("remote_resource");
        names.add("received_timestamp");
        names.add("send_timestamp");
        names.add("receipt_server_timestamp");
        names.add("receipt_device_timestamp");
        names.add("read_device_timestamp");
        names.add("played_device_timestamp");
        names.add("raw_data");
        names.add("recipient_count");
        names.add("participant_hash");
        names.add("media_enc_hash");
        names.add("starred");
        names.add("quoted_row_id");
        names.add("mentioned_jids");
        names.add("multicast_id");
        names.add("edit_version");

        return names;
    }
}
