package com.example.francesco.testservices.Utils;

import android.util.Log;

import com.example.francesco.testservices.Controllers.Controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Logger {

    public static synchronized void log(String TAG, String data, Boolean addToLogCat){
        if(Controller.isTestEnvironment){
            Log.w(TAG,data);
            return;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, HH:mm:ss.SSS");
        Calendar cal = Calendar.getInstance();
        FileUtilities.saveData(sdf.format(cal.getTime()) + ": [" + TAG + "]: " + data, Controller.getLogPath(), true );
        if(addToLogCat){
            Log.i(TAG,data);
        }
    }

}
