package com.example.francesco.testservices.Services;

import android.app.IntentService;
import android.content.Intent;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.ServiceScheduler;
import com.example.francesco.testservices.Utils.TimeUtilities;

import java.util.Calendar;

/**
 *
 * This service gets started by GetAndSaveMessagesServices.
 * If the queue is empty, it does nothing, cause it means that there are no messages that are yet
 * to be sent. If the queue has some elements in it, the service gets the first one, sends it,
 * saves its "id" in a file, wich name is given by FileUtilities.getSentMessagesIdsFileName() method,
 * and then reschedule itself. If the message is not sent properly, the service throws an exception
 * and puts the message back in the queue.
 */

public class SendMessagesService extends IntentService {

    private static String TAG = "SendMessages";

    public SendMessagesService() {
        super("SendMessagesService");
    }
    public static Long maxNextMessageOffset= 15*60*1000L;

    public void onHandleIntent(Intent intent) {
        Boolean atLeastOneMessageHasBeenProcessed = false;
        Controller controller = Controller.getInstance();
        WhatsappManager whatsappManager = controller.getWhatsappManager();
        try{
            ServiceScheduler.cancelAlarms(getApplicationContext(), this.getClass());
            controller.sendMessagesServiceExecutions++;
            MessagesQueue queue = controller.getMessagesQueue();
            while(!queue.isEmpty()){
                WhatsappMessage messageToSend = queue.getMessage(0);
                Long date = messageToSend.getDate();
                if(date < Calendar.getInstance().getTimeInMillis()+ 60000){
                    tryToSendMessage(messageToSend, whatsappManager, Controller.getSentIdsFilePath()
                    );
                    atLeastOneMessageHasBeenProcessed = true;
                    queue.pop();
                    Logger.log(TAG,"Message was removed from the queue",true);
                } else{
                    Logger.log(TAG, "Next message send time:" + TimeUtilities.formatMillisToDate(date), true);
                    Long offSet = date - Calendar.getInstance().getTimeInMillis();
                    scheduleNextActivation(offSet);
                    break;
                }
            }
        } catch(Exception e){
            Logger.log("Error:", e.getMessage(), true);
        } if(atLeastOneMessageHasBeenProcessed){
            Logger.log(TAG,"atLeastOneMessageHasBeenProcessed: true",true);
            controller.openAndKillWhatsapp(getApplicationContext());
            controller.messagesHaveBeenSent();
        } else Logger.log(TAG,"atLeastOneMessageHasBeenProcessed: false",true);
    }


    public static void tryToSendMessage(WhatsappMessage messageToSend, WhatsappManager whatsappManager, String sentIdsFilePath){
        try {
            Logger.log(TAG, "There is a message to send. Id:"+messageToSend.getId()+"; Time:"+TimeUtilities.formatMillisToDate(messageToSend.getDate()), true);
            if (whatsappManager.send(messageToSend)) {
                Logger.log(TAG, "The message was inserted and hopefully sent. key_id: " + messageToSend.getWhatsappKey()
                        +"  Status: "+messageToSend.getStatusStr(), true);
                FileUtilities.saveData(messageToSend.getId()+" "+messageToSend.getWhatsappKey(), sentIdsFilePath, true);
                Thread.sleep(1001);
            } else {
                do{
                    Logger.log(TAG, "Retrying...", true);
                    messageToSend.decreaseAttemptsleft();
                    Thread.sleep(5000);
                } while(messageToSend.getAttemptsLeft() > 0 && !whatsappManager.send(messageToSend));
                if(messageToSend.hasBeenSent())
                    FileUtilities.saveData(messageToSend.getId()+" "+messageToSend.getWhatsappKey(), sentIdsFilePath, true);
            }
        } catch(Exception e){
            Logger.log(TAG, e.getMessage(), true);
        }
    }

    public void scheduleNextActivation(Long offSet){
        try{
            if( offSet > maxNextMessageOffset){
                Logger.log(TAG,"Calculated offset is bigger than the maximum value allowed." +
                        "Scheduling GetAndSaveMessages next...", true);
                Long nextActviationTime = Controller.getElapsedRealTime() + maxNextMessageOffset;
                ServiceScheduler.scheduleService(getApplicationContext(),
                        GetAndSaveMessagesService.class, nextActviationTime);
            } else {
                Logger.log(TAG,"Offset is ok. Scheduling SendMessages next...", true);
                Long nextActviationTime = Controller.getElapsedRealTime() + offSet;
                ServiceScheduler.scheduleService(getApplicationContext(),
                        SendMessagesService.class, nextActviationTime);
            }
        } catch(Exception e){
            Logger.log(TAG,e.getMessage(), true);
        }
    }



}
