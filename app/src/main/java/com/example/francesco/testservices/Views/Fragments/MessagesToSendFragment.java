package com.example.francesco.testservices.Views.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.R;
import com.example.francesco.testservices.Views.MessagesAdapter;

/**
 * Fragment molto semplice che mostra all'interno di una listView i messaggi in coda
*/

public class MessagesToSendFragment extends Fragment {

    public MessagesToSendFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_messages_to_send, container, false);

    }

    public void onStart(){
        super.onStart();
        boolean setListView = Controller.getInstance().thereAreMessagesToSend();
        if(setListView){
            ListView lv = (ListView) getView().findViewById(R.id.messagesListView);
            lv.setAdapter(new MessagesAdapter(getContext(), Controller.getInstance().getMessagesQueue()));
        }
    }


}
