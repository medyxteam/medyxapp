package com.example.francesco.testservices.Model;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Utils.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * This class represents the queue of messages that are yet to be sent on a specific day.
 * The messages are instances of the "MessagesToSend" class, wich an extensions of "Serializable", and
 * are grouped  inside the "messages" array.
 * The key method of this class is "setQueue": it initialize the "messages" array by comparing the
 * ids of the messages inside the "dailyMessages.json" file with the ones inside the "sentMessagesIds" file.
 * Every message with and id that is not included in the sentMessagesIds file gets put inside the queue.
 */

public class MessagesQueue implements Serializable {

    private static String TAG = "MessagesQueue";
    private static String TAG_SET_QUEUE = "MessagesQueue/setQueue";
    private ArrayList<WhatsappMessage> messages;

    public MessagesQueue() {
        this.messages = new ArrayList<>();
    }

    public synchronized void setQueue(JSONArray dailyMessagesJson, String lastSentMessage_Id, Long currentTime, Long message_maxNegativeOffsetAllowed,Controller controller, Context c) {

        try {
            this.messages = new ArrayList<>();
            String id;
            String message;
            String number;
            Long date;
   //         Toast.makeText(c, "set Queue", Toast.LENGTH_SHORT).show();
            if (lastSentMessage_Id == null)
                    Logger.log(TAG_SET_QUEUE, "No messages sent today", true);
            for (int i = 0; i < dailyMessagesJson.length(); i++) {
                JSONObject jsonMessage = dailyMessagesJson.getJSONObject(i);
                try {
                    id = jsonMessage.getString("id");
                    if (id.equals("")){
                        Logger.log(TAG_SET_QUEUE,"id is blank; this message will be skipped",true);
                        continue;
                    }
                } catch(Exception e){
                    Logger.log(TAG_SET_QUEUE,"id not found; this message will be skipped",true);
                    continue;
                }if (lastSentMessage_Id != null && Long.parseLong(id) <= Long.parseLong(lastSentMessage_Id) ) {
                    //do nothing. message with this id has been sent and must not be added to the queue
                } else {
                    try {
                        number = jsonMessage.getString("patientNumber");
                    } catch(Exception e){
                        Logger.log(TAG_SET_QUEUE,"patientNumber not found/valid; this message will be skipped",true);
                        Logger.log(TAG_SET_QUEUE,e.getMessage(),true);
                        continue;
                    }try{
                        message = jsonMessage.getString("message");
                    } catch(Exception e){
                        Logger.log(TAG_SET_QUEUE,"message field not found/valid; this message will be skipped",true);
                        Logger.log(TAG_SET_QUEUE,e.getMessage(),true);
                        continue;
                    }try{
                        date = jsonMessage.getLong("date");
                    } catch(Exception e){
                        Logger.log(TAG_SET_QUEUE,"date field not found/valid; this message will be skipped",true);
                        Logger.log(TAG_SET_QUEUE,e.getMessage(),true);
                        continue;
                    }//Toast.makeText(c, message, Toast.LENGTH_SHORT).show();
                        if(controller.datesAreTheSame(date, currentTime)) {
                        Long offset = date - currentTime;
                        if(offset >= message_maxNegativeOffsetAllowed ){
                          WhatsappMessage messageToSend = new WhatsappMessage(message, number, date + 600, id);
                            if(!this.containsMessage(id)) {
                                this.addMessage(messageToSend);
                                Log.i(TAG_SET_QUEUE, "messageToSend added to queue");
                            }
                        } else
                            Logger.log(TAG_SET_QUEUE, "More than 30 min have passed since the message had to be delivered." +
                                    "It won't be added to the queue", true);
                    } else {
                        Logger.log(TAG_SET_QUEUE, "the date of the message is not today! It won't be added to the queue", true);
                    }
                }
            }
            Logger.log(TAG_SET_QUEUE, "Queue is set. Length:" + this.messages.size(), true);
        }
        catch (Exception e) {
            Logger.log(TAG_SET_QUEUE, e.toString(), true);
        }
    }
    public WhatsappMessage getNextMessageToSend(){
        if(!this.isEmpty())
            return this.getMessages().get(0);
        else
            return null;
    }
    public ArrayList<WhatsappMessage> getMessages() {
        return this.messages;
    }
    public WhatsappMessage getMessage(int index){
     try{
         return this.messages.get(index);
     }catch(Exception e){
         return null;
     }
    }
    public int size(){
        try{
            return this.getMessages().size();
        }
        catch (Exception e){
            return 0;
        }
    }
    public synchronized void addMessage(WhatsappMessage m) {
        this.messages.add(m);
    }
    public boolean isEmpty() {
        return this.messages.size()==0;
    }
    public synchronized WhatsappMessage pop() {
        try{
            return this.messages.remove(0);
        }
        catch(Exception e){
            Log.w(TAG,"Queue is probably empty.");
            return null;
        }
    }
    public boolean containsMessage(String id){
        boolean result= false;
        for(WhatsappMessage m : messages){
            if(m.getId().equals(id))
                result = true;
        }
        return result;
    }






}


