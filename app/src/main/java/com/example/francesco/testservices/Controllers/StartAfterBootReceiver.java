package com.example.francesco.testservices.Controllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.francesco.testservices.Services.GetAndSaveMessagesService;
import com.example.francesco.testservices.Utils.Logger;



public class StartAfterBootReceiver extends BroadcastReceiver {
    public static String TAG = "StartAfterBootReceiver";
    @Override
    public void onReceive(Context context, Intent intent){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Logger.log(TAG,"Entered inside StartAfterBootReceiver", true);
        if(!pref.getBoolean("startServicesAfterBoot", false)) {
            return;
        }
        Logger.log(TAG,"Phone boot completed", true);
        Logger.log(TAG,"Proceeding to start GetAndSaveMessagesService ...", true);
        Intent getDailyFileIntent = new Intent(context, GetAndSaveMessagesService.class);
        context.startService(getDailyFileIntent);

    }
}
