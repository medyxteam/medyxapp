package com.example.francesco.testservices;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Services.GetAndSaveMessagesService;
import com.example.francesco.testservices.Services.SendMessagesService;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.ServiceScheduler;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.util.Calendar;
import java.util.HashSet;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class EndToEndInstrumentedTest {
    Class getAndSaveClassName = GetAndSaveMessagesService.class;
    Class sendMessagesClassName = SendMessagesService.class;
    Context appContext = InstrumentationRegistry.getTargetContext();
    Intent GET_Intent = new Intent(appContext, getAndSaveClassName);
    Intent send_Intent = new Intent(appContext, sendMessagesClassName);

    @Before
    public void initialize(){
        Controller.isTestEnvironment = true;
        TestUtilities.deleteOldFiles();
        FileUtilities.createLogAndTestFolders();
        if(!BuildConfig.FLAVOR.equals("development")){
            fail("Not development environment");
        }
    }

    @Test
    public void correctFlowTest(){
        try {
        /**
         *  Scatta la mezzanotte. GetAndSaveMessages sta per avviarsi, il giorno prima sono stati mandati messaggi
         *  e quindi esistono sentIds e dailyMessages
         */
            TestUtilities.createPastDayMessagesFiles();
        /**
         * Il server è pronto per ricevere la chiamata del telefono, in quanto ci sono die messaggi da inviare
         * Una volta lanciato il servizio, il file verrà scaricato, la post verrà eseguita(ovvero esisterà un file
         * all'interno della cartella 'POST_logs' del disposivo un log con la data di ieri (es.'2017_05_27.json')
             * Esisterà anche un log all'interno della cartella 'GENERAL_logs' con il log del giorno prima (es.'2017_05_27.txt')
             */

            appContext.startService(GET_Intent);
            Thread.sleep(20000);
            ServiceScheduler.cancelAlarms(appContext,getAndSaveClassName);
            ServiceScheduler.cancelAlarms(appContext,sendMessagesClassName);
            String dailyMessagesFilePath  = Controller.getStoragePath()+Controller.getDailyMessagesFileName();
            String pastDayLogPath = Controller.getStoragePath()+"GENERAL_logs/"+Controller.getPastDay()+".txt";
            String pastDayPOSTLogPath = Controller.getStoragePath()+"POST_logs/"+Controller.getPastDay()+".json";
            assertTrue(new File(dailyMessagesFilePath)
                                .exists() &&
                            new File(pastDayLogPath)
                                .exists() &&
                            new File(pastDayPOSTLogPath)
                                .exists());
            /**
             * Ci deve essere almeno un messaggio da mandare, quindi la coda non è vuota
             * (simuliamo questa situazione aggiungendo manualmente un messaggio)
             */
            MessagesQueue mq = Controller.getInstance().getMessagesQueue();
            Long timeToSend = Calendar.getInstance().getTimeInMillis() + 5000;
            WhatsappMessage waMex = new WhatsappMessage("test","393450154761",
                    timeToSend,"05" );
            mq.getMessages().add(0,waMex);
            assertFalse(Controller.getInstance().getMessagesQueue().isEmpty());

            /**
             * Non sappiamo se sono stati mandati dei messaggi(perchè non conosciamo a priori il file ricevuto dal server)
             * (lanciamo in ogni caso il servizio per assicurarci che almeno quello che abbiamo insierto manualmente venga inviato)
             */

            Long offset = Calendar.getInstance().getTimeInMillis() -
                    mq.getMessage(0).getDate();
            assertTrue( offset > -30000 && offset < 30000);
            appContext.startService(send_Intent);

            JSONArray messagesToSend = new JSONArray(FileUtilities
                    .getData(Controller.getStoragePath() + Controller.getDailyMessagesFileName()));
            Thread.sleep(15000* messagesToSend.length());

            assertTrue(Controller.getInstance().sendMessagesServiceExecutions > 0);
            ServiceScheduler.cancelAlarms(appContext,getAndSaveClassName);
            ServiceScheduler.cancelAlarms(appContext,sendMessagesClassName);
            /**
             * Se sono stati inviati dei messaggi, all'interno del file 'sentMessagesIds' devono essere presenti
             * tutti i relativi Medyx Id + Whatsapp Key Id
             */

            HashSet<String> sentIdsSet = TestUtilities.
                    generateSentMessagesIdsSet(Controller.getStoragePath()+Controller.getSentIdsFileName());


            JSONObject manualMessage = new JSONObject();
            manualMessage.put("id","05");
            manualMessage.put("date",timeToSend);
            messagesToSend.put(manualMessage);

                /**
                 * Tutti gli ID dei messaggi 'validi' dal file giornaliero (piu quello inserito manualmente) devono
                 * essere presenti all'interno del file sentMessagesIds
                  */

            for(int i = 0; i<messagesToSend.length(); i++){
                JSONObject obj = messagesToSend.getJSONObject(i);
                if(obj.getLong("date") < Calendar.getInstance().getTimeInMillis() &&
                        obj.getLong("date") > Calendar.getInstance().getTimeInMillis() + Controller.message_maxNegativeOffsetAllowed
                        ){
                    assertTrue(sentIdsSet.contains(obj.getString("id")));
                    sentIdsSet.remove(obj.getString("id"));
                }
            }
            assertTrue(sentIdsSet.isEmpty());
        }
        catch(Exception e){
            fail(e.toString());
        }
    }

    @Test
    public void invalidUrlFlowTest(){
        /**
         * Scatta la mezzanotte, inizia un nuovo flow. Il giorno prima sono stati inviati dei messaggi
         * e quindi anche il file dailyMessages esiste sul dispositivo
         */
        TestUtilities.createPastDayMessagesFiles();
        /**
         * Il servizio GetAndSaveMessages è schedulato e parte poco dopo la mezzanotte;
         * per qualche motivo però il server è irraggiungibile, oppure la risorsa non è presente.
         * Ciò implica che nessun file giornaliero venga nè salavato nè tantomento scaricato in seguito
         * all'esecuzione del servizio.
         */
        Intent GETIntent = new Intent(appContext,getAndSaveClassName);
        GETIntent.putExtra("GET_urlCode",TestUtilities.URL_GET_INVALID);
        appContext.startService(GETIntent);
        ServiceScheduler.cancelAlarms(appContext,sendMessagesClassName);
        ServiceScheduler.cancelAlarms(appContext,sendMessagesClassName);
        try{
            Thread.sleep(15000);
        } catch(Exception e){
            fail(e.getMessage());
        }
        String dailyMessagesFilePath  = Controller.getStoragePath()+Controller.getDailyMessagesFileName();
        String pastDayLogPath = Controller.getStoragePath()+"GENERAL_logs/"+Controller.getPastDay()+".txt";
        String pastDayPOSTLogPath = Controller.getStoragePath()+"POST_logs/"+Controller.getPastDay()+".json";
        String sentIdsFilePath = Controller.getSentIdsFilePath();
        assertTrue(!new File(dailyMessagesFilePath)
                .exists() &&
                new File(pastDayLogPath)
                        .exists() &&
                new File(pastDayPOSTLogPath)
                        .exists() &&
                !new File(sentIdsFilePath)
                        .exists() &&
                Controller.getInstance().getMessagesQueue().isEmpty());
        assertTrue(Controller.getInstance().getAndSaveServiceExecutions == 1);
        assertTrue(Controller.getInstance().sendMessagesServiceExecutions == 0);
    }

    @Test
    public void emptyArrayResponseFlow(){
        /**
         * Scatta la mezzanotte, inizia un nuovo flow. Il giorno prima sono stati inviati dei messaggi
         * e quindi anche il file dailyMessages esiste sul dispositivo
         */
        TestUtilities.createPastDayMessagesFiles();
        /**
         * Il servizio GetAndSaveMessages è schedulato e parte poco dopo la mezzanotte;
         * esso chiede le informazioni al server che però risponde con un JSON composto da un array vuoto
         * Ciò implica che la coda PROVA a settarsi ma rimane comunque vuota in quanto non trova alcun
         * messaggio al'interno del file. Avrà quindi dimensione 0
         */
        Intent GETIntent = new Intent(appContext,getAndSaveClassName);
        GETIntent.putExtra("GET_urlCode",TestUtilities.URL_GET_BLANK_JSON);
        appContext.startService(GETIntent);
        ServiceScheduler.cancelAlarms(appContext,getAndSaveClassName);
        ServiceScheduler.cancelAlarms(appContext,sendMessagesClassName);
        try{
            Thread.sleep(15000);
        } catch(Exception e){
            fail(e.getMessage());
        }
        String dailyMessagesFilePath  = Controller.getStoragePath()+Controller.getDailyMessagesFileName();
        String pastDayLogPath = Controller.getStoragePath()+"GENERAL_logs/"+Controller.getPastDay()+".txt";
        String pastDayPOSTLogPath = Controller.getStoragePath()+"POST_logs/"+Controller.getPastDay()+".json";
        assertTrue(new File(dailyMessagesFilePath)
                .exists() &&
                new File(pastDayLogPath)
                        .exists() &&
                new File(pastDayPOSTLogPath)
                        .exists());
        assertTrue(Controller.getInstance().getMessagesQueue().isEmpty());
        assertTrue(Controller.getInstance().getAndSaveServiceExecutions == 1);
        assertTrue(Controller.getInstance().sendMessagesServiceExecutions == 0);
    }

    @Test
    public void messagesAreSentCorrectlyOverTime(){
        /**
         * La mezzanotte è passata; il dailyMessagesFile è stato già scaricato, e la coda è non vuota
         * ciò significa che ci sono messaggi da mandare e che l'esecuzione di GetAndSaveMessages
         */
        try{
            JSONArray messagesArray =TestUtilities.buildSmallDelaysArray();
            assertTrue(FileUtilities
                    .saveData(messagesArray.toString(),Controller.getStoragePath()+
                            Controller.getDailyMessagesFileName(),false));

            Controller controller = Controller.getInstance();
            controller.setMessagesQueue();

            Intent GETIntent = new Intent(appContext,getAndSaveClassName);

            appContext.startService(GETIntent);
            //Thread.sleep(2000000);
            Thread.sleep(22* 1000);
            assertTrue(controller.sendMessagesServiceExecutions == 1 && controller.getAndSaveServiceExecutions ==1);
            Thread.sleep(90*1000);
            assertTrue(controller.sendMessagesServiceExecutions == 2 && controller.getAndSaveServiceExecutions ==1);


            /**
             * I messaggi sono stati mandati tutti, quindi gli id scritti nel file sentMessagesIds
             * devono essere uguali a quelli all'interno di 'messagesArray'
             *
             */
            HashSet<String> hSet = TestUtilities.generateSentMessagesIdsSet(Controller.getSentIdsFilePath());
            assertEquals(hSet.size(),messagesArray.length());
            for(int i = 0; i< messagesArray.length(); i++){
                JSONObject m = messagesArray.getJSONObject(i);
                assertTrue(hSet.contains(m.getString("id")));
            }


        }
        catch(Exception e){

        }
    }

    @Test
    public void messageExceedsMaxAllowedOffsetAndServicesAreScheduled(){
        /**
         * Mezzanotte è passata; il file dei messaggio giornalieri è stato recuperato, ma il primo messaggio in coda
         * a più di 15 minuti dal momento in cui viene processato; ciò implica che entrambi i servizi verranno
         * eseguiti una volta soltanto, ma il file sentIdsFile sarà vuoto, perchè nessun messaggio verrà inviato
         */
        try{
            JSONArray messagesArray =TestUtilities.buildBigDelaysArray();
            assertTrue(FileUtilities
                    .saveData(messagesArray.toString(),Controller.getStoragePath()+
                            Controller.getDailyMessagesFileName(),false));

            Controller controller = Controller.getInstance();
            controller.setMessagesQueue();

            Intent GETIntent = new Intent(appContext,getAndSaveClassName);

            appContext.startService(GETIntent);
            //Thread.sleep(2000000);

            Thread.sleep(90*1000);
            assertTrue(controller.sendMessagesServiceExecutions == 1 && controller.getAndSaveServiceExecutions ==1);
            assertTrue(controller.isGetAndSaveServiceScheduled && controller.isSendMessagesServiceScheduled);
            assertFalse(new File(Controller.getSentIdsFilePath()).exists());
        }
        catch(Exception e){

        }
    }

    @Test
    public void POST_isNotExecutedWhenSentIdsDoesNotExist(){
        /**
         * E' appena scattata la mezzanotte; la richiesta GET non è stata ancora effettuata; ciò implica che
         * dovrebbe essere eseguita la POST riguardante il report giornaliero. In questo caso però il file sentIds
         * non esiste, quindi la POST non viene eseguita affatto
         */
        try{
            assertFalse(new File(Controller.getSentIdsFilePath()).exists());
            Intent GETIntent = new Intent(appContext,getAndSaveClassName);
            appContext.startService(GETIntent);
            Thread.sleep(20*1000);
            String pastDayLogPath = Controller.getStoragePath()+"GENERAL_logs/"+Controller.getPastDay()+".txt";
            assertFalse(new File(pastDayLogPath).exists());
            assertTrue(Controller.getInstance().isGetAndSaveServiceScheduled);
        }
        catch(Exception e){
            fail(e.getMessage());
        }
    }


}
