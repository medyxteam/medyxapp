package com.example.francesco.testservices;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Services.SendMessagesService;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.ServiceScheduler;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Calendar;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


public class SendMessagesServiceInstrumentedTest {

    Context appContext = InstrumentationRegistry.getTargetContext();
    WhatsappManager whatsappManager;

    @Before
    public void initalize(){
        Controller.isTestEnvironment = true;
        TestUtilities.deleteOldFiles();
        whatsappManager = new WhatsappManager(Controller.getWaDbPath(), Controller.getInstance().getCommandManager());;
        try{
            Thread.sleep(3000);
        }
        catch(Exception e){
            fail(e.getMessage());
        }
    }



    @Test
    public void aMessageIsSentAndItsIdsIsWritten() {
        String sentIdsFilePath = Controller.getStoragePath() + Controller.getSentIdsFileName();
        try{
            Thread.sleep(3000);
        } catch(Exception e){
            fail(e.getMessage());
        }
        assertFalse(new File(sentIdsFilePath).exists());
        JSONArray testArray = TestUtilities.buildTestArray();
        Controller controller = Controller.getInstance();
        assertTrue(controller.getMessagesQueue().isEmpty());
        try{
            controller.getMessagesQueue()
                    .setQueue(
                            testArray,
                            null,
                            Calendar.getInstance().getTimeInMillis(),
                            Controller.message_maxNegativeOffsetAllowed,
                            Controller.getInstance()
            );
        } catch(java.lang.NullPointerException e){
            Logger.log("sendMessageTest",e.getMessage(),true);
        }
        assertFalse(controller.getMessagesQueue().isEmpty());
        SendMessagesService.tryToSendMessage(controller.getMessagesQueue().getNextMessageToSend(), whatsappManager, Controller.getSentIdsFilePath());
        assertTrue(new File(sentIdsFilePath).exists());
        JSONArray sentIdsArray;
        try {
            sentIdsArray = new JSONObject(FileUtilities.generateIdsAndStatusJsonStr(whatsappManager,sentIdsFilePath)).getJSONArray("ids");
            JSONObject sentMessage = sentIdsArray.getJSONObject(0);
             assertEquals(sentMessage.getString("id"), controller.getMessagesQueue().getNextMessageToSend().getId());
        } catch (Exception e) {
            fail();
        }
    }


    @Test
    public void noMessagesSentWhenQueueIsEmpty(){

        try{
            Thread.sleep(3000);
        }
        catch(Exception e){
            fail();
        }
        TestUtilities.deleteOldFiles();

        assertTrue(Controller.getInstance().getMessagesQueue().isEmpty());
        Intent sendMessagesIntent = new Intent(appContext, SendMessagesService.class);
        appContext.startService(sendMessagesIntent);
        try{
            Thread.sleep(5000);
        } catch (Exception e){}
        assertFalse(new File(Controller.getStoragePath() + Controller.getSentIdsFileName()).exists());
    }


    @Test
    public void idsFileExists_moreMessagesAreSent(){
        String sentIdsFilePath = Controller.getStoragePath() + Controller.getSentIdsFileName();
        try{
            Thread.sleep(3000);
        }
        catch(Exception e){
            fail();
        }

        TestUtilities.deleteOldFiles();
        assertFalse(new File(sentIdsFilePath).exists());
        JSONArray testArray = TestUtilities.buildTestArray();
        Controller controller = Controller.getInstance();
        assertTrue(controller.getMessagesQueue().isEmpty());
        controller.getMessagesQueue()
                .setQueue(
                        testArray,
                        null,
                        Calendar.getInstance().getTimeInMillis(),
                        Controller.message_maxNegativeOffsetAllowed,
                        Controller.getInstance()
                );
        assertFalse(controller.getMessagesQueue().isEmpty());
        int initialQueueSize = controller.getMessagesQueue().size();
        for(int i=0; i< initialQueueSize; i++){
            WhatsappMessage m = controller.getMessagesQueue().pop();
            SendMessagesService.tryToSendMessage(m, whatsappManager,Controller.getSentIdsFilePath());
        }
        assertTrue(new File(sentIdsFilePath).exists());
        assertTrue(controller.getMessagesQueue().isEmpty());
        long messageDate =Calendar.getInstance().getTimeInMillis()+6000;
        long activationTime = messageDate - Calendar.getInstance().getTimeInMillis();
        WhatsappMessage newMessage = new WhatsappMessage("final message of the test","393450154761",
                messageDate,"testMessage");
        controller.getMessagesQueue().addMessage(newMessage);
        assertFalse(controller.getMessagesQueue().isEmpty());
        ServiceScheduler.scheduleService(appContext,SendMessagesService.class,activationTime);
        try{
            Thread.sleep(16000);
            JSONArray sentIdsArray = new JSONObject(FileUtilities.generateIdsAndStatusJsonStr(whatsappManager,sentIdsFilePath)).getJSONArray("ids");
            boolean searchOutput = false;
            String idToFind = "testMessage";
            for(int i = 0; i < sentIdsArray.length(); i++){
                JSONObject sentMessage = sentIdsArray.getJSONObject(i);
                String sentMessageId= sentMessage.getString("id");
                if(idToFind.equals(sentMessageId)){
                    searchOutput= true;
                }
            }
            assertTrue(searchOutput);
        }
        catch(Exception e){
            fail("");
        }


    }

}
