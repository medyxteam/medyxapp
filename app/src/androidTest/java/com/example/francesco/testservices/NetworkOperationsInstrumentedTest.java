package com.example.francesco.testservices;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.NetworkManager;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import RootUtilities.Shell;
import RootUtilities.SimpleCommand;
import okhttp3.Response;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

@RunWith(AndroidJUnit4.class)
public class NetworkOperationsInstrumentedTest {
    Context appContext = InstrumentationRegistry.getTargetContext();
    WhatsappManager whatsappManager;
    @Before
    public void initalize(){
        Controller.isTestEnvironment = true;
        TestUtilities.deleteOldFiles();
        whatsappManager = new WhatsappManager(Controller.getWaDbPath(), Controller.getInstance().getCommandManager());;
    }



    @Test
    public void networkCheckWorks(){
        try{

            Shell shell = Shell.startRootShell();
            shell.add(new SimpleCommand("settings put global airplane_mode_on 1")).waitForFinish();
            shell.add(new SimpleCommand("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true")).waitForFinish();
            Thread.sleep(3000);
            assertFalse(NetworkManager.isNetworkAvailable(appContext));
            shell.add(new SimpleCommand("settings put global airplane_mode_on 0")).waitForFinish();
            shell.add(new SimpleCommand("am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false")).waitForFinish();
            Thread.sleep(20000);
            assertTrue(NetworkManager.isNetworkAvailable(appContext));
            shell.close();
        }
        catch(Exception e){
            fail(e.getMessage());
        }
    }


    @Test
    public void POST_URL_isCorrect() throws Exception{
        if(BuildConfig.FLAVOR.equals("development")){
            assertEquals(appContext.getString(R.string.postReportUrl_developmentValue),
                    NetworkManager.getUrl(NetworkManager.URL_POST_REPORT, appContext));
        } else {
            assertEquals(appContext.getString(R.string.postReportUrl_productionValue),
                    NetworkManager.getUrl(NetworkManager.URL_POST_REPORT, appContext));
        }

    }

    @Test
    public void GET_URL_isCorrect() throws Exception {
        if(BuildConfig.FLAVOR.equals("development")){
            assertEquals(appContext.getString(R.string.getMessagesUrl_developmentValue),
                    NetworkManager.getUrl(NetworkManager.URL_GET_MESSAGES_WITHOUT_PARAMS, appContext));
        } else {
            assertEquals(appContext.getString(R.string.getMessagesUrl_productionValue),
                    NetworkManager.getUrl(NetworkManager.URL_GET_MESSAGES_WITHOUT_PARAMS, appContext));
        }

    }

    @Test
    public void GET_URL_paramsAreAddedCorrectly() throws Exception {
        String[] paramNames = {"ciao","sono", "lollo"};
        String[] values = {"1","2","3"};
        String url = NetworkManager.getUrl(NetworkManager.URL_GET_MESSAGES_WITHOUT_PARAMS,appContext);
        assertEquals(NetworkManager.addParametersToUrl(paramNames,values,url),url.concat("?ciao=1&sono=2&lollo=3"));
    }

    @Test
    public void GET_URL_paramsAreNotAddedCorrectly() throws Exception {
        String[] paramNames = {"ciao","sono", "lollo"};
        String[] values = {"1","2"};
        String url = NetworkManager.getUrl(NetworkManager.URL_GET_MESSAGES_WITHOUT_PARAMS,appContext);
        assertEquals(NetworkManager.addParametersToUrl(paramNames,values,url),url);
    }

    @Test
    public void addParametersToUrl_returnsBasicUrl_whenParametersAreIncorrect(){
        String[] paramNames = {"ciao","sono", "lollo"};
        String[] values = null;
        String url = NetworkManager.getUrl(NetworkManager.URL_GET_MESSAGES_WITHOUT_PARAMS,appContext);
        assertEquals(NetworkManager.addParametersToUrl(paramNames,values,url),url);

    }

    @Test
    public void okHtpp_GET_fails_with_IncorrectUrl(){
        String url = "www.google.it";
        try{
            Response response = NetworkManager.okHtpp_Get(url);
            assertFalse(response.isSuccessful());
        }
        catch(Exception e){
            assertTrue(true);
        }


    }


    @Test
    public void okHtpp_GET_WorksProperly(){
        String url = "http://www.google.it";
        try{
            Response response = NetworkManager.okHtpp_Get(url);
            assertTrue(response.isSuccessful());
        } catch(Exception e){
            fail();
        }
    }

    @Test
    public void okHtpp_GET_gives404(){
        String url = "http://www.google.it/ciao";
        try{
            Response response = NetworkManager.okHtpp_Get(url);
            assertTrue(response.code() == 404);
        }
        catch(Exception e){
            fail();
        }
    }

    @Test
    public void messagesAreRetrieved_with_correct_Url(){
        String url = NetworkManager.getUrl(NetworkManager.URL_GET_MESSAGES_WITHOUT_PARAMS,appContext );
        assertTrue( url != null);
        String result = NetworkManager.makeGetRequest(url);
        assertTrue( result != null);
    }

    @Test
    public void messagesAreRetrieved_with_correct_Url_plus_parameters(){
        String url = NetworkManager.getUrl(NetworkManager.URL_GET_MESSAGES,appContext );
        assertTrue( url != null);
        String result = NetworkManager.makeGetRequest(url);
        assertTrue( result != null);
    }


    @Test
    public void idsAreSentToServerSuccessfully(){
        Controller.isTestEnvironment = true;
        TestUtilities.deleteOldFiles();
        generateSentMessagesIdsFile(TestUtilities.fakeIdsFilePath);
        assertTrue( NetworkManager
                .sendYesterdayIdsToServer(
                    NetworkManager.getUrl(NetworkManager.URL_POST_REPORT, appContext),
                    FileUtilities.generateIdsAndStatusJsonStr(whatsappManager,TestUtilities.fakeIdsFilePath),
                    Controller.getPhoneNumber(appContext),
                    2
                ) != null
        );
    }




    public void generateSentMessagesIdsFile(String sentIdsFilePath){
        FileUtilities.saveData("01 xxx",sentIdsFilePath, true);
        FileUtilities.saveData("02",sentIdsFilePath, true);
        FileUtilities.saveData("03 xxx xxx",sentIdsFilePath, true);
        // vanno creati test sul file degli ids e sulla stringa generata per la post.
        //quindi prima test sull'invio dei messaggi WA
    }



}
