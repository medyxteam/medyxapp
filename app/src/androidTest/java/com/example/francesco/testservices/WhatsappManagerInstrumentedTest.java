package com.example.francesco.testservices;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Utils.CommandManager;
import com.example.francesco.testservices.Utils.CommandOutputSaver;
import com.example.francesco.testservices.Utils.NetworkManager;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Calendar;

import RootUtilities.SimpleCommand;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


public class WhatsappManagerInstrumentedTest {

    Context appContext = InstrumentationRegistry.getTargetContext();
    String[] realPhoneNumber = {"393892192591"};
    String[] devicePhoneNumber = {"393450154761"};
    String message = "this is a test";
    String whatsappDbPath = appContext.getString(R.string.whatsappMessagesDbPath);
    WhatsappManager whatsappManager;
    CommandManager commandManager;
    CommandOutputSaver commandOutputSaver;
    @Before
    public void initalize(){
        Controller.isTestEnvironment = true;
        TestUtilities.deleteOldFiles();
        whatsappManager = new WhatsappManager(Controller.getWaDbPath(),Controller.getInstance().getCommandManager());;
        commandManager = Controller.getInstance().getCommandManager();
        commandOutputSaver = Controller.getInstance().getCommandManager().getCommandOutputSaver();
        try{
            Thread.sleep(2000);
        }
        catch(Exception e){
        }
    }



    @Test
    public void msgstoreDatabaseExists(){
        assertTrue(new File("/data/data/com.whatsapp/databases/msgstore.db").exists());
    }

    @Test
    public void messagesTableExists(){
        String query = "select name from sqlite_master where type='table'";
        whatsappManager.queryMessagesTable(query, CommandOutputSaver.SAVE_ALL);//non funziona perchè va
        //implementata la chiamata al commandManager all'interno di queryMessagesTable
        String output = Controller.getInstance().getCommandManager().getCommandOutputSaver().getSavedOutput();
        Controller.getInstance().getCommandManager().getCommandOutputSaver().clearSavedOutput();
        assertTrue(output.contains("messages ") || output.contains(" messages"));
    }

    @Test
    public void commandManagerTest(){
        try {
            String query = "select name from sqlite_master where type='table'";
            SimpleCommand command1 = new SimpleCommand("ps | grep -w 'com.whatsapp' | awk '{print $2}' | xargs kill");
            SimpleCommand command2 = new SimpleCommand("sqlite3 /data/data/com.whatsapp/databases/msgstore.db \"" + query + "; \" ");
            commandManager.executeSingleCommand(command1, CommandOutputSaver.SAVE_NONE, false);
            commandManager.executeSingleCommand(command2, CommandOutputSaver.SAVE_ALL, true);
            String output = Controller.getInstance().getCommandManager().getCommandOutputSaver().getSavedOutput();
            assertTrue(output.contains("messages ") || output.contains(" messages"));
        }
        catch(Exception e){
            fail(e.getMessage());
        }
    }

    /*
    @Test
    public void messagesTable_fieldsDidNotChange(){
        String query = "pragma table_info(messages)";
        String dbPath = appContext.getString(R.string.whatsappMessagesDbPath);
        SQLiteDatabase db = SQLiteDatabase.openDatabase(dbPath,null,0);
        Cursor c = db.rawQuery(query,null);
        HashSet<String> names = TestUtilities.buildColumnNamesSet();
        if(c.moveToFirst()){
            for(int i = 0; i < c.getCount() ; i++){
                String currentName = c.getString(1);
                if(!names.contains(currentName)){
                    System.out.println("");
                    fail("More columns found in the table");
                }
                c.moveToNext();
            }
        }
    }
*/
    @Test
    public void queryOnMessagesTableGivesSomeOutput(){
        String query = "select * from messages limit 1";
        whatsappManager.queryMessagesTable(query, CommandOutputSaver.SAVE_ALL);
        String output = Controller.getInstance().getCommandManager().getCommandOutputSaver().getSavedOutput();
        assertFalse(output.equals(""));
        assertFalse(output.contains("Error"));
    }




    @Test
    public void messageIsInserted(){

        //assertTrue(Controller.openAndKillWhatsapp(appContext));
        WhatsappMessage mess = new WhatsappMessage(message,realPhoneNumber[0],Calendar.getInstance().getTimeInMillis(),"01");
        whatsappManager.send(mess);
        assertTrue(mess.hasBeenSent());

    }


    @Test
    public void messageIsSent(){

        assertTrue(WhatsappManager.isPhoneNumberValid(realPhoneNumber[0]));
        WhatsappMessage mess = new WhatsappMessage(message,realPhoneNumber[0],Calendar.getInstance().getTimeInMillis(),"01");
        whatsappManager.send(mess);
        assertTrue(mess.getWhatsappKey() != null);
        try{
            Thread.sleep(15000);
        }
        catch( Exception e){

        }

        String output = whatsappManager.retrieveStatus(mess.getWhatsappKey());
        assertFalse(output.equals("") || output.contains("Error"));
        if(NetworkManager.isNetworkAvailable(appContext)){
            assertTrue(output.equals("4") ||  output.equals("5") || output.equals("13"));
        }
        else
            assertEquals(output,"0");
    }


    @Test
    public void multipleMessagesAreNotInsertedWhenInTheSameSecond(){
        assertTrue(WhatsappManager.isPhoneNumberValid(devicePhoneNumber[0]));
        Long t1  = java.util.Calendar.getInstance().getTimeInMillis();

        WhatsappMessage mess1 = new WhatsappMessage("this message should come alone",devicePhoneNumber[0], java.util.Calendar.getInstance().getTimeInMillis(),"01");
        whatsappManager.send(mess1);
        assertTrue(mess1.getWhatsappKey()!= null);
        WhatsappMessage mess2 = new WhatsappMessage(message,devicePhoneNumber[0], java.util.Calendar.getInstance().getTimeInMillis(),"02");
        whatsappManager.send(mess2);
        String whatsappKey2 = mess2.getWhatsappKey();
        Long t2 = java.util.Calendar.getInstance().getTimeInMillis();
        if(t2 - t1 < 1000 ){
            assertTrue( whatsappKey2==null);
        } else{
            assertFalse(whatsappKey2==null);
        }
    }
    @Test
    public void multipleMessagesAreInsertedAfterOneSecond(){

        assertTrue(WhatsappManager.isPhoneNumberValid(devicePhoneNumber[0]));
        WhatsappMessage mess1 = new WhatsappMessage("this message should be followed by another one",devicePhoneNumber[0], java.util.Calendar.getInstance().getTimeInMillis(),"01");
        whatsappManager.send(mess1);
        String whatsappKey1 = mess1.getWhatsappKey();
        assertTrue(whatsappKey1 != null);
        try{
            Thread.sleep(1000);
        }
        catch(Exception e){

        }
        assertTrue(WhatsappManager.isPhoneNumberValid(devicePhoneNumber[0]));
        WhatsappMessage mess2 = new WhatsappMessage("this is the second message. well done",devicePhoneNumber[0],Calendar.getInstance().getTimeInMillis(),"02");
        whatsappManager.send(mess2);
        String whatsappKey2 = mess2.getWhatsappKey();
        assertTrue(whatsappKey2!=null);
    }

    @Test
    public void messageIsNotInsertedCauseNumberIsNotValid(){
        String invalidPhoneNumber = "39345";
        WhatsappMessage mess = new WhatsappMessage("this is the second message. well done",invalidPhoneNumber,Calendar.getInstance().getTimeInMillis(),"02");
        whatsappManager.send(mess);
        String whatsappKey = mess.getWhatsappKey();
        assertEquals(whatsappKey, null);
        assertFalse(mess.hasBeenSent());
    }

    @Test
    public void messageIsNotInsertedCauseTextIsBlank(){
        String invalidPhoneNumber = "39345";
        WhatsappMessage mess = new WhatsappMessage("t",invalidPhoneNumber, Calendar.getInstance().getTimeInMillis(),"03");
        whatsappManager.send(mess);
        String whatsappKey = mess.getWhatsappKey();
        assertEquals(whatsappKey, null);
        assertFalse(mess.hasBeenSent());
    }

    @Test
    public void statusIsRetrievedCorrectly(){
        try{
            String waKey = "1495190981--1150867590";
            String status = whatsappManager.retrieveStatus(waKey);
            assertFalse(status == null && status.equals("111") );
        }
        catch(Exception e){
            fail(e.getMessage());
        }

    }



}
