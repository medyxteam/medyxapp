package com.example.francesco.testservices;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.NetworkManager;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


public class FileOperationsInstrumentedTest {
    String realPhoneNumber = "393892192591";
    String fakeIdsFilePath = Controller.getStoragePath() + "fakeSentIds.txt";
    Context context = InstrumentationRegistry.getTargetContext();
    WhatsappManager whatsappManager;
    @Before
    public void initalize(){
        Controller.isTestEnvironment = true;
        whatsappManager = new WhatsappManager(Controller.getWaDbPath(),Controller.getInstance().getCommandManager());;
        TestUtilities.deleteOldFiles();
    }



    @Test
    public void messageIsSent_jsonStrIsGeneratedCorrectly(){
        FileUtilities.deleteFile(fakeIdsFilePath);
        WhatsappMessage mess1 = new WhatsappMessage("this message should come alone",realPhoneNumber, java.util.Calendar.getInstance().getTimeInMillis(),"01");
        whatsappManager.send(mess1);
        assertTrue(mess1.hasBeenSent());
        String waKey = mess1.getWhatsappKey();
        FileUtilities.saveData("01 "+waKey,fakeIdsFilePath, true);
        assertTrue(NetworkManager.isNetworkAvailable(InstrumentationRegistry.getTargetContext()));
        try{
            Thread.sleep(6000);
            String jsonStr = FileUtilities.generateIdsAndStatusJsonStr(whatsappManager,fakeIdsFilePath);
            JSONArray jsonArray = new JSONObject(jsonStr).getJSONArray("ids");

            JSONObject elem = jsonArray.getJSONObject(0);
            assertTrue(elem.getString("status").equals("0")
                    || elem.getString("status").equals("4")
                    || elem.getString("status").equals("5")
                    || elem.getString("status").equals("13"));

            try{
                elem = jsonArray.getJSONObject(1);
            }
            catch(Exception e){
                assertTrue(true);
            }
        }
        catch (Exception e){
            fail();
        }
    }

    @Test
    public void multipleMessagesAreSent_jsonStrGeneratedCorrectly(){
        FileUtilities.deleteFile(fakeIdsFilePath);
        FileUtilities.saveData("01 xxx",fakeIdsFilePath, true);
        FileUtilities.saveData("02",fakeIdsFilePath, true);
        FileUtilities.saveData("03 xxx xxx",fakeIdsFilePath, true);
        WhatsappMessage mess1 = new WhatsappMessage("this is a test","393450154761", Calendar.getInstance().getTimeInMillis(),"01");
        whatsappManager.send(mess1);
        WhatsappMessage mess2 = new WhatsappMessage("this is a test","393450154761", Calendar.getInstance().getTimeInMillis(),"01");
        whatsappManager.send(mess2);
        String waKey1 = mess1.getWhatsappKey();
        String waKey2 = mess2.getWhatsappKey();
        FileUtilities.saveData("04 "+waKey1,fakeIdsFilePath, true);
        FileUtilities.saveData("05 "+waKey2,fakeIdsFilePath, true);


        try{
            Thread.sleep(6000);
            String jsonStr = FileUtilities.generateIdsAndStatusJsonStr(whatsappManager,fakeIdsFilePath);
            JSONArray jsonArray = new JSONObject(jsonStr).getJSONArray("ids");

            JSONObject elem = jsonArray.getJSONObject(0);
            assertEquals(elem.getString("status"),"111");

            elem = jsonArray.getJSONObject(1);
            assertEquals(elem.getString("status"),"109");

            elem = jsonArray.getJSONObject(2);
            assertEquals(elem.getString("status"),"tuttoaputtane");

            elem = jsonArray.getJSONObject(3);
            assertEquals(elem.getString("status"),"0");

            elem = jsonArray.getJSONObject(4);
            assertEquals(elem.getString("status"),"0");

            try{
                elem = jsonArray.getJSONObject(5);
            }
            catch(Exception e){
                assertTrue(true);
            }
        }


        catch (Exception e){
            fail();
        }
    }


    @Test
    public void lastSentIdsIsRetrivedCorrectly(){
        FileUtilities.deleteFile(fakeIdsFilePath);
        FileUtilities.saveData("01 xxx",fakeIdsFilePath, true);
        FileUtilities.saveData("02",fakeIdsFilePath, true);
        FileUtilities.saveData("03 xxx xxx",fakeIdsFilePath, true);
         String lastId = FileUtilities.getFirstWordOfLastLine(fakeIdsFilePath);
        assertEquals(lastId,"03");
    }

}
