package com.example.francesco.testservices;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Utils.CommandManager;
import com.example.francesco.testservices.Utils.CommandOutputSaver;

import org.junit.Before;
import org.junit.Test;

import RootUtilities.SimpleCommand;

import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;

/**
 * Created by francesco on 21/06/2017.
 */

public class CommandManagerInstrumentedTest {
    CommandManager commandManager;

    @Before
    public void setup(){
        commandManager = Controller.getInstance().getCommandManager();
    }

    @Test
    public void commandManagerTest(){
        try {
            String query = "select name from sqlite_master where type='table'";
            SimpleCommand command1 = new SimpleCommand("ps | grep -w 'com.whatsapp' | awk '{print $2}' | xargs kill");
            SimpleCommand command2 = new SimpleCommand("sqlite3 /data/data/com.whatsapp/databases/msgstore.db \"" + query + "; \" ");
            commandManager.executeSingleCommand(command1, CommandOutputSaver.SAVE_NONE, false);
            commandManager.executeSingleCommand(command2, CommandOutputSaver.SAVE_ALL, true);
            String output = commandManager.getCommandOutputSaver().getSavedOutput();
            assertTrue(output.contains("messages ") || output.contains(" messages"));
        }
        catch(Exception e){
            fail(e.getMessage());
        }
    }
}
