package com.example.francesco.testservices;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.Utils.FileUtilities;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;


public class MessagesQueueInstrumentedTest {


    @Before
    public void initalize(){
        Controller.isTestEnvironment = true;
        TestUtilities.deleteOldFiles();
    }

    @Test
    public void canChangeMessageToSendFromOutsideTheQueue(){
        MessagesQueue mq = new MessagesQueue();
        WhatsappMessage m = new WhatsappMessage("ciao","393450154761",1235l,"01");
        mq.addMessage(m);
        WhatsappMessage m2 = mq.getNextMessageToSend();
        m2.decreaseAttemptsleft();
        assertEquals(mq.getNextMessageToSend().getAttemptsLeft(),m2.getAttemptsLeft(),m.getAttemptsLeft());
    }

    @Test
    public void messageIsAddedCorrectlyAndPopReturnsCorrectValue(){
        MessagesQueue mq = new MessagesQueue();
        WhatsappMessage m = new WhatsappMessage("mm","33",12345l,"01");
        int mqInitialSize= mq.size();
        mq.addMessage(m);
        assertTrue(mq.size() > mqInitialSize);
        assertEquals(mq.pop(), m);
        assertEquals(mq.size(),mqInitialSize);
    }

    @Test
    public void sizeIsCorrect(){
        WhatsappMessage m1 = new WhatsappMessage("this","33333333333" ,123456l,"01" );
        WhatsappMessage m2 = new WhatsappMessage("is","33333333333" ,123456l,"02" );
        WhatsappMessage m3 = new WhatsappMessage("a test","33333333333" ,123456l,"03" );
        MessagesQueue mq = new MessagesQueue();
        assertTrue(mq.isEmpty());
        assertTrue(mq.size()==0);
        mq.addMessage(m1);
        mq.addMessage(m2);
        mq.addMessage(m3);
        assertTrue(mq.size() == 3);
    }

    @Test
    public void queueIsSetProperly() throws Exception {
        JSONArray testArray = TestUtilities.buildTestArray();
        assertFalse(testArray == null);
        MessagesQueue testQueue = new MessagesQueue();
        FileUtilities.deleteFile(Controller.getStoragePath()+Controller.getSentIdsFileName());
        testQueue.setQueue(new JSONArray(testArray.toString()), null, Calendar.getInstance().getTimeInMillis(),Controller.message_maxNegativeOffsetAllowed,Controller.getInstance());
        assertFalse(testQueue.isEmpty());
        assertTrue(TestUtilities.queueContainsValidElementsOnly(testArray,testQueue));
    }

    @Test
    public void popIsSynchronized(){
        WhatsappMessage m1 = new WhatsappMessage("ciao","33333333333" ,123456l,"01" );
        WhatsappMessage m2 = new WhatsappMessage("francesco","33333333333" ,123456l,"02" );
        final MessagesQueue mq = new MessagesQueue();
        mq.addMessage(m1);
        mq.addMessage(m2);
        mq.addMessage(m2);
        mq.addMessage(m2);
        mq.addMessage(m2);
        System.out.println("initial size: "+mq.size());

        new Thread(new Runnable() {
            public void run(){
                try{
                    System.out.println("Thread 1" + "id: "+mq.pop().getId() + "size: "+ mq.size());
                } catch(Exception e){
                }
            }
        }).start();


        new Thread(new Runnable() {
            public void run(){
                try{
                    System.out.println("Thread 2" + "id: "+mq.pop().getId() + "size: "+ mq.size());
                } catch(Exception e){
                }
            }
        }).start();

        new Thread(new Runnable() {
            public void run(){
                try{
                    System.out.println("Thread 3" + "id: "+mq.pop().getId() + "size: "+ mq.size());
                } catch(Exception e){
                }
            }
        }).start();

        new Thread(new Runnable() {
            public void run(){
                try{
                    System.out.println("Thread 4" + "id: "+mq.pop().getId() + "size: "+ mq.size());
                } catch(Exception e){
                }
            }
        }).start();

        new Thread(new Runnable() {
            public void run(){
                try{
                    System.out.println("Thread 5" + "id: "+mq.pop().getId() + "size: "+ mq.size());
                } catch(Exception e){
                }
            }
        }).start();

        try{
            Thread.sleep(5000);
        }
        catch (Exception e){

        }
        assertTrue(mq.size() ==0);
    }

    @Test
    public void addMessageIsSynchronized(){
        final WhatsappMessage m1 = new WhatsappMessage("this","33333333333" ,123456l,"01" );
        final WhatsappMessage m2 = new WhatsappMessage("is","33333333333" ,123456l,"02" );
        final WhatsappMessage m3 = new WhatsappMessage("a test","33333333333" ,123456l,"03" );
        final MessagesQueue mq = new MessagesQueue();

        assertTrue(mq.isEmpty());

        mq.addMessage(m1);
        assertTrue(mq.size()==1);
        new Thread(new Runnable() {
            public void run(){
                if(!mq.containsMessage(m1.getId())){
                    mq.addMessage(m1);
                }
            }
        }).start();

        new Thread(new Runnable() {
            public void run(){
                if(!mq.containsMessage(m1.getId())){
                    mq.addMessage(m1);
                }
            }
        }).start();

        assertTrue(mq.size() ==1);


    }


}