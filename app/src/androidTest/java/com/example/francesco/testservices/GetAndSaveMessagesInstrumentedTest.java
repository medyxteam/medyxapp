package com.example.francesco.testservices;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Services.GetAndSaveMessagesService;
import com.example.francesco.testservices.Services.SendMessagesService;
import com.example.francesco.testservices.Utils.ServiceScheduler;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;


public class GetAndSaveMessagesInstrumentedTest {
    Class getAndSaveClassName = GetAndSaveMessagesService.class;
    Class sendMessagesClassName = SendMessagesService.class;
    Context appContext = InstrumentationRegistry.getTargetContext();
    Intent intent = new Intent(appContext, getAndSaveClassName);

    @Before
    public void initalize(){
        Controller.isTestEnvironment = true;
        TestUtilities.deleteOldFiles();
    }

    @Test
    public void dailyMessagesIsNullAndIsRetrieved(){
        String dailyFilePath = Controller.getStoragePath()+ Controller.getDailyMessagesFileName();
        assertFalse(new File(dailyFilePath).exists());
        appContext.startService(intent);
        try{
            Thread.sleep(30000);
        } catch(Exception e){
            fail();
        }
        ServiceScheduler.cancelAlarms(appContext, getAndSaveClassName);
        ServiceScheduler.cancelAlarms(appContext, sendMessagesClassName);
        assertTrue(new File(dailyFilePath).exists());
    }
}
