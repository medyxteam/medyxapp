package com.example.francesco.testservices;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Utils.CommandManager;
import com.example.francesco.testservices.Utils.CommandOutputSaver;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.ShellWrapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import RootUtilities.Command;
import RootUtilities.Shell;
import RootUtilities.SimpleCommand;

import static junit.framework.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.when;


/**
 * Created by francesco on 21/06/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(Logger.class)
public class CommandManagerUnitTest {

    @Mock
    ShellWrapper shellWrapper;
    @Mock
    Shell shell;
    @Mock
    SimpleCommand command;
    @Mock
    CommandOutputSaver commandOutputSaver;
    @Mock
    Command command1;

    CommandManager commandManager;
    @Before
    public void setup(){
        Controller.isTestEnvironment = true;
        PowerMockito.mockStatic(Logger.class);
        commandManager = new CommandManager(shellWrapper,commandOutputSaver);
        doNothing().when(commandOutputSaver).setSaveMode(anyInt());
        //doNothing().when(commandOutputSaver).setSaveMode(anyInt());
        when(shellWrapper.startRootShell()).thenReturn(shell);
    }

    @Test
    public void testSingleCommandExecutionLogic(){
        try{
            when(shell.add(any(SimpleCommand.class))).thenReturn(command1);
            doNothing().when(command1).waitForFinish();
            doNothing().when(shell).close();

            commandManager.executeSingleCommand(command,1,true);
        }
        catch(Exception e){
            fail(e.getMessage());
        }
    }
}
