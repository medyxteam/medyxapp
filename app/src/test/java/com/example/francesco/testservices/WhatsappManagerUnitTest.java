package com.example.francesco.testservices;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.WhatsappManager;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Utils.CommandManager;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.ShellWrapper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import RootUtilities.Command;
import RootUtilities.Shell;
import RootUtilities.SimpleCommand;
import exceptions.GeneralException;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by francesco on 15/06/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(Logger.class)
public class WhatsappManagerUnitTest {
    @Mock
    ShellWrapper shellWrapper;
    @Mock
    Shell shell;
    @Mock
    SimpleCommand sCommand;
    @Mock
    Command command;
    @Mock
    WhatsappMessage waMessage;
    @Mock
    CommandManager commandManager;

    WhatsappManager whatsappManager;
    String fakeDbPath = "asds";
    String fakeNumber = "393450154761";
    String fakeMsg = "ciao";
    String validKey_id = "12314155";
    String fakeStatus = "0";
    @Before
    public void setup(){
        try{
            Controller.isTestEnvironment = true;
            PowerMockito.mockStatic(Logger.class);
            whatsappManager = new WhatsappManager(fakeDbPath,commandManager);
            doNothing().when(commandManager).executeSingleCommand(any(SimpleCommand.class),anyInt(),anyBoolean());
        }
        catch (Exception e){
        }
    }

    @Test
    public void testSendWhatsappMessage() throws GeneralException{
        try{
            when(shellWrapper.startRootShell()).thenReturn(shell);
            when(shell.add(any(Command.class))).thenReturn(sCommand);

            doNothing().when(sCommand).waitForFinish();
            doNothing().when(shell).close();

            String key_id = whatsappManager.sendWhatsappMessage(fakeNumber,fakeMsg);
            assertTrue( key_id != null ) ;
        }
        catch(Exception e){
            fail(e.getMessage());
        }

    }


    @Test
    public void testSendMessage_messageMayHaveNotBeenInserted() throws GeneralException {
        try{
            when(shellWrapper.startRootShell()).thenReturn(shell);
            when(shell.add(any(Command.class))).thenReturn(sCommand);
            doNothing().when(sCommand).waitForFinish();
            doNothing().when(shell).close();
            //WhatsappManager.messageHasBeenInserted = false;
            String key_id = whatsappManager.sendWhatsappMessage(fakeNumber,fakeMsg);
            assertTrue( key_id != null ) ;
        }
        catch(Exception e){
            fail(e.getMessage());
        }
    }

    @Test
    public void phoneNumberCheckWorks(){
        String longNumber = "1234568901111";
        String shortNumber = "1234";
        String validNumber_shorter = "12345678901";
        String validNumber_longer = "123456789012";
        assertFalse(WhatsappManager.isPhoneNumberValid(longNumber));
        assertFalse(WhatsappManager.isPhoneNumberValid(shortNumber));
        assertTrue(WhatsappManager.isPhoneNumberValid(validNumber_longer));
        assertTrue(WhatsappManager.isPhoneNumberValid(validNumber_shorter));
    }

    @Test
    public void testSendDoesNotFail() {
        try {
            when(shellWrapper.startRootShell()).thenReturn(shell);
            when(shell.add(any(Command.class))).thenReturn(sCommand);
            doNothing().when(sCommand).waitForFinish();
            doNothing().when(shell).close();
            when(waMessage.hasBeenSent()).thenReturn(false);
            doNothing().when(waMessage).setWhatsappKey(anyString());
            when(waMessage.getNumber()).thenReturn(fakeNumber);
            when(waMessage.getMessage()).thenReturn(fakeMsg);
            String key_id = whatsappManager.sendWhatsappMessage(fakeNumber, fakeMsg);
            assertTrue(key_id != null);
            when(waMessage.getWhatsappKey()).thenReturn(key_id);
            doNothing().when(waMessage).setStatus(anyString());
            when(waMessage.getStatusStr()).thenReturn(fakeStatus);
            assertTrue(whatsappManager.send(waMessage));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

        @Test
        public void testSendFails () {
            try {
                when(shellWrapper.startRootShell()).thenReturn(shell);
                when(shell.add(any(Command.class))).thenReturn(sCommand);
                doNothing().when(sCommand).waitForFinish();
                doNothing().when(shell).close();
                when(waMessage.hasBeenSent()).thenReturn(false);
                doNothing().when(waMessage).setWhatsappKey(anyString());
                when(waMessage.getNumber()).thenReturn(fakeNumber);
                when(waMessage.getMessage()).thenReturn(fakeMsg);
                String key_id = whatsappManager.sendWhatsappMessage(fakeNumber, fakeMsg);
                assertTrue(key_id != null);
                when(waMessage.getWhatsappKey()).thenReturn(key_id);
                doNothing().when(waMessage).setStatus(anyString());
                when(waMessage.getStatusStr()).thenReturn("55");
                assertFalse(whatsappManager.send(waMessage));
            } catch (Exception e) {
                fail(e.getMessage());
            }

        }


    }