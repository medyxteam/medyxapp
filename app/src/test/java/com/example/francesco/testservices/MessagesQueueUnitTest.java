package com.example.francesco.testservices;

import com.example.francesco.testservices.Controllers.Controller;
import com.example.francesco.testservices.Model.MessagesQueue;
import com.example.francesco.testservices.Model.WhatsappMessage;
import com.example.francesco.testservices.Utils.Logger;
import com.example.francesco.testservices.Utils.TestUtilities;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Calendar;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by francesco on 16/06/2017.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Logger.class,TestUtilities.class,Controller.class})
public class MessagesQueueUnitTest {

    public MessagesQueue mq;
    public String fakeId_1 = "01";
    public String fakeId_2 = "02";
    public String fakeId_3 = "03";
    public String fakeId_4 = "04";
    Long currentTime;
    WhatsappMessage m1,m2,m3,m4;
    JSONArray testArray;

    @Before
    public void setup(){
        PowerMockito.mockStatic(Logger.class);
        mq = new MessagesQueue();
        m1 = new WhatsappMessage("ciao","bello",12345l,"01");
        mq.addMessage(m1);
        m2 = new WhatsappMessage("ciao","bello",12345l,"02");
        mq.addMessage(m2);
        m3 = new WhatsappMessage("ciao","bello",12345l,"03");
        mq.addMessage(m3);
        m4 = new WhatsappMessage("ciao","bello",12345l,"04");
        currentTime = Calendar.getInstance().getTimeInMillis();

    }

    @Test
    public void messageIsAddedProperly(){
        mq.addMessage(m4);
        assertEquals(mq.getMessage(mq.size()-1),m4);
    }


    @Test
    public void queueContainsMessage(){
        assertTrue(mq.containsMessage(m1.getId()) &&
                   mq.containsMessage(m2.getId()) &&
                   mq.containsMessage(m3.getId())
        );
    }

    @Test
    public void queueDoesntCointainMessage(){
        assertFalse(mq.containsMessage(fakeId_4));
    }

    @Test
    public void queueReturnsCorrectElements(){
        assertEquals(mq.getMessage(-1),null);
        assertEquals(mq.getMessage(0),m1);
        assertEquals(mq.getMessage(1),m2);
        assertEquals(mq.getMessage(2),m3);
        assertEquals(mq.getMessage(4),null);
    }

    


}
