package com.example.francesco.testservices;

import com.example.francesco.testservices.Utils.CommandOutputSaver;
import com.example.francesco.testservices.Utils.Logger;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

/**
 * Created by francesco on 21/06/2017.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(Logger.class)
public class CommandOutputSaverUnitTest {
    CommandOutputSaver commandOutputSaver;
    String str1 = "str1";
    String str2 = "str2";
    String str3 = "str3";
    String blank = "";
    int unexistingMode = -10;
    @Before
    public void setup(){
        commandOutputSaver = new CommandOutputSaver(CommandOutputSaver.SAVE_LAST);
    }

    @Test
    public void testIncorrectSetSaveMode(){
        int initialSaveMode = commandOutputSaver.getSaveMode();
        commandOutputSaver.setSaveMode(unexistingMode);
        int finalSaveMode  = commandOutputSaver.getSaveMode();
        assertEquals(initialSaveMode,finalSaveMode);
    }

    @Test
    public void testCorrectSetSaveMode(){
        int initialSaveMode = commandOutputSaver.getSaveMode();
        commandOutputSaver.setSaveMode(CommandOutputSaver.SAVE_NONE);
        int finalSaveMode  = commandOutputSaver.getSaveMode();
        assertFalse(initialSaveMode == finalSaveMode);
    }

    @Test
    public void testSaveAll(){
        commandOutputSaver.setSaveMode(CommandOutputSaver.SAVE_ALL);
        commandOutputSaver.saveOutput(str1);
        commandOutputSaver.saveOutput(str2);
        String expectedString = " "+str1+" "+str2;
        assertEquals(commandOutputSaver.getSavedOutput(),expectedString);
    }

    @Test
    public void testSaveLast(){

        commandOutputSaver.saveOutput(str1);
        commandOutputSaver.saveOutput(str2);
        commandOutputSaver.saveOutput(str3);
        assertEquals(commandOutputSaver.getSavedOutput(),str3);
    }

    @Test
    public void testSaveNone(){
        commandOutputSaver.setSaveMode(CommandOutputSaver.SAVE_NONE);
        commandOutputSaver.saveOutput(str1);
        commandOutputSaver.saveOutput(str2);
        commandOutputSaver.saveOutput(str3);
        assertEquals(commandOutputSaver.getSavedOutput(),blank);
    }

    @Test
    public void testClearSavedOutput(){
        commandOutputSaver.setSaveMode(CommandOutputSaver.SAVE_ALL);
        commandOutputSaver.saveOutput(str1);
        commandOutputSaver.saveOutput(str2);
        commandOutputSaver.saveOutput(str3);
        commandOutputSaver.clearSavedOutput();
        assertEquals(commandOutputSaver.getSavedOutput(),blank);
    }




}
